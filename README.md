# BUGWUpytools

## Installation
```
cd directory_where_you_want_this_code_to_end_up
git clone https://gitlab.com/Sevans7/bugwupytools BUGWUpytools
cd BUGWUpytools
pip install -e .
```
(Note the `.` at the end of the pip install command. That `.` means "here, at the current directory")

After running those lines, you're ready to open up python and `import BUGWUpytools as bp`

(You might need to `pip install pygsheets` first.)

[TODO] requirements.txt file, for automatically installing dependencies

## Usage
See https://gitlab.com/Sevans7/bugwupytools/-/tree/main/examples

## Contributing
Accepting any contributions! Happy to dicuss and incorporate changes to the code.

[TODO] auto-testing (CI/CD)

## License
[TODO]

## Project status
Under development / in use.

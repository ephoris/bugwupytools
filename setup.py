"""
Created on Thu Feb 09 2023

@author: Sevans
"""

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
   long_description = fh.read()

setup(
      name     = 'BUGWUpytools',
      version  = '0.1',
      author   = 'Sam Evans',
      author_email = 's7evans11@gmail.com',
      long_description=long_description,
      long_description_content_type='text/markdown',
      url      = 'https://gitlab.com/Sevans7/bugwupytools',
      packages = find_packages()
      )
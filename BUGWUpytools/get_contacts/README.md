# get_contacts

Scripts used to fetch contact infos.

## boa.py

`boa.py` is an .OAB file parser originally from
[github.com/byteDJINN/BOA](https://github.com/byteDJINN/BOA),
slightly modified to fit our needs.

This can be used to parse the Offline Global Address List file (.OAB file) that
Outlook stores locally to find a list of names and emails for everyone in our
institution.

### How to Use

You must first find the correct .OAB file from Outlook :

1. In Outlook, open the Address Book.
2. Right click on "Offline Global Address List" and go to properties.
3. A path to a folder is listed under "The current server is:". Go to the folder.
4. `udetails.oab` is the one we're interested in.

Run `boa.py` on `udetails.oab`. (By default, you can simply copy `udetails.oab`
into the same folder and run `python boa.py`)

It will create .csv and .pkl files of all GivenNames, Surnames and Emails.

## getemails.py

Script to fill .xlsx files with emails from an address book.

### How to use

By default it will use the `contact.pkl` file (created by `boa.py`)
as the address book.

Pass in an excel file that contains first and last names:

```
python getemails.py PATH/TO/FILE.xlsx
```

A new file, `PATH/TO/FILE_filled.xlsx`, will be created,
with new emails filled in and a new column of the log:

 * NEW : Found email that matches name, was not previously in excel file.
 * CONFIRMED : Found email that matches name, was previously in excel file.
 * ERR : There was some error in matching name, email and/or pre-existing
    email. Description will be provided.

It may be necessary to modify, in the code, the column numbers that correspond
to first, last name and email columns.


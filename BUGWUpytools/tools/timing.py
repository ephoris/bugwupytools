"""
File Purpose: misc. tools related to time.
"""

import datetime

from .pytools import format_docstring


DEFAULT_TIME_FORMAT = "%Y-%m-%d_%H-%M-%S"
_time_format_docs = \
    '''default: YYYY-mm-dd_HH-MM-SS.
    E.g. 2023-02-11_09-39-34 for year=2023, month=2, day=11, hour=9, minute=39, second=34.'''

def datetime_now():
    '''return now as a datetime.'''
    return datetime.datetime.now()

@format_docstring(timeformatdocs = _time_format_docs)
def strtime(datetime_, format=DEFAULT_TIME_FORMAT):
    '''return string representing datetime_, in the format provided.
    {timeformatdocs}
    '''
    return datetime_.strftime(format)

@format_docstring(timeformatdocs = _time_format_docs)
def strtime_now(format=DEFAULT_TIME_FORMAT):
    '''return string representing the current time, in the format provided.
    {timeformatdocs}
    '''
    return strtime(datetime_now(), format=format)

@format_docstring(timeformatdocs = _time_format_docs)
def strptime(s, format=DEFAULT_TIME_FORMAT):
    '''return datetime representing string, assuming the string format provided.
    {timeformatdocs}
    '''
    return datetime.datetime.strptime(s, format)

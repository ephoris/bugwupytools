"""
File Purpose: tools for pygsheets_tools
"""

import os
from ..fonts import FONT_PATH_LOOKUP
from ..imports import ImportFailed

try:
    import googleapiclient as gac
except ImportError:
    gac = ImportFailed('googleapiclient')

try:
    import matplotlib.colors as mpl_colors
except ImportError:
    mpl_colors = ImportFailed('matplotlib.colors')

try:
    import PIL
except ImportError:
    PIL = ImportFailed('PIL')


def is_Exception__no_conditional_format_found(x):
    '''returns whether x is an exception corresponding to "No conditional format on sheet" error,
    which occurs when requesting to remove conditional formatting rule but none are found.
    returns whether isinstance(x, googleapiclient.errors.HttpError) and 
        "Invalid requests[0].deleteConditionalFormatRule: No conditional format on sheet" in str(x)
    '''
    if not isinstance(x, gac.errors.HttpError):
        return False
    return "Invalid requests[0].deleteConditionalFormatRule: No conditional format on sheet" in str(x)

def hex_to_rgb_dict(x):
    '''converts hex string x to a dict of {'red': red_value, 'green': green_value, 'blue': blue_value}.'''
    r, g, b = mpl_colors.to_rgb(x)
    return dict(red=r, green=g, blue=b)

class Batching():
    '''context manager for batch updates for a SheetsChart.
    Behavior depends on passthrough, and value of sheet.batch_mode upon entry.
    Default passthrough=True.

    If (sheet.batch_mode was False, on entry) or (not passthrough):
        (on entry) clears batch
        (on entry) set sheet.batch_mode = True
        (on exit) set sheet.batch_mode = False
        (on exit without error) run_batch
        (on exit with error) clear batch

    If (sheet.batch_mode was True, on entry) and (passthrough):
        does nothing on entry & exit.
        Useful if this Batching() might be inside another Batching();
            in that case, let the external Batching() handle running the batch,
            so we can batch more commands before sending the batch request to google.

    Note: batching is handled by client, not the sheet,
        so it's best to only work on one sheet per `with` block.
    '''
    def __init__(self, sheet, passthrough=True, **kw__run_batch):
        self.sheet = sheet
        self.passthrough = passthrough
        self.kw__run_batch = kw__run_batch

    def __enter__(self):
        self.batch_was = (self.sheet.batch_mode)
        self._passing_through = (self.batch_was and self.passthrough)
        if not self._passing_through:
            self.sheet.clear_batch()
            self.sheet.batch_mode = True

    def __exit__(self, exc_type, exc_value, traceback):
        if not self._passing_through:
            self.sheet.batch_mode = False
            if exc_type is None:  # no error
                self.sheet.run_batch(**self.kw__run_batch)
            else:  # got an error
                self.sheet.clear_batch()

def fontsize_pixels(text, font='arial', font_size=10, *, font_path=None):
    '''tells size of font in pixels.
    font: string name for font.
        key passed to FONT_PATH_LOOKUP.
        options: 'arial', 'arial_bold'
    font_path: None or str
        if provided, use this .ttf file instead of font.
    '''
    if font_path is None:
        font_path = FONT_PATH_LOOKUP[font]
    image = PIL.Image.new("RGB", (1, 1))
    draw = PIL.ImageDraw.Draw(image)
    font_obj = PIL.ImageFont.truetype(font_path, font_size)
    width = draw.textlength(text, font=font_obj)
    return width

def headersize_pixels(text, filter=True, *, w1=1.3, w2=20):
    '''returns guess about size of header col for text.
    Includes space for filter arrow, if filter==True.
    '''
    return fontsize_pixels(text, 'arial_bold') * w1 + (w2 if filter else 0)

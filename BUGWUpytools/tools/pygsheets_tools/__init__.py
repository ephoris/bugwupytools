"""
Package Purpose: custom additions to pygsheets functionality.

These are intended to be "portable",
i.e. not related to any specific tasks from the main package,
and easily copy-pastable into other packages.

This file:
Imports the main important objects throughout this subpackage.
"""

import pygsheets   # this is required for ANY functionality in this subpackage to be relevant.

from .custom_pygsheets import (
    open_spreadsheet, open_sheets_doc,
    SheetsDoc, SheetsChart,
)
from .pygsheets_tools_tools import (
    is_Exception__no_conditional_format_found,
    hex_to_rgb_dict, Batching,
    fontsize_pixels,
)
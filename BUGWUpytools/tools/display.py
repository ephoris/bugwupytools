"""
File Purpose: tools related to displaying things.
"""

import re 

from .imports import ImportFailed
try:
    import IPython.display as ipd
except ImportError:
    ipd = ImportFailed('IPython.display')

from .timing import strtime_now


_RE_URL_PATTERN = re.compile(r'https?://\S+')

def _replace_with_clickable_url(match):
    '''replaces match with html so that it can be clicked, when displayed with IPython.display.HTML.
    This function is intended to be passed to re.sub. See also: replace_with_clickable_urls()
    '''
    url = match.group(0)
    return '<a href="{0}" target="_blank">{0}</a>'.format(url)

def replace_with_clickable_urls(s):
    '''replaces urls in str(s) with html so that they can be clicked,
        when displayed with IPython.display.HTML.
    See also: display_with_urls_clickable()
    '''
    return re.sub(_RE_URL_PATTERN, _replace_with_clickable_url, str(s))

def format_for_html(s):
    '''formats str(s) to be displayed as html.'''
    s = str(s)
    return f'<pre>{s}</pre>'  # preserves whitespace

def display_with_clickable_urls(s):
    '''displays str(s) with urls clickable.'''
    s0 = replace_with_clickable_urls(s)
    s1 = format_for_html(s0)
    ipd.display(ipd.HTML(s1))

clickable_urls = display_with_clickable_urls

class Logger():
    '''naive logging implementation.
    provides log_and_print() which logs the message to self._log and also prints it.
    Also provides log() which just logs the message to self._log.
    self._log is a list of strings.
    self.save(name) saves to file '{name}_{time}.txt', or just name if name ends with '.txt'.
        default name is 'Log'.
    '''
    def __init__(self, filename='Log'):
        self.reset()
        self.filename = filename

    def reset(self):
        '''sets self._log = []'''
        self._log = []

    def log(self, *msg, end='\n'):
        '''appends [*(str(m) for m in msg), end] to self.log.'''
        self._log += [*(str(m) for m in msg), end]

    def log_and_print(self, *msg, end='\n', **kw__print):
        '''prints msg, then appends to self.log'''
        print(*msg, end=end, **kw__print)
        self.log(*msg, end=end)

    def as_string(self):
        r'''returns self._log as string, i.e. ''.join(self._log).'''
        return ''.join(self._log)

    def save(self, filename=None, mode='a'):
        '''save log to file {filename}_{time}.txt, or just filename if filename ends with .txt.
        if filename is None use self.filename. default: 'Log'.
        mode can be 'a' or 'w', it is the mode for open(filename, mode=mode).
        '''
        if filename is None:
            filename = self.filename
        if not filename.endswith('.txt'):
            filename = f'{filename}_{strtime_now()}.txt'
        logstr = self.as_string()
        with open(filename, mode=mode) as file:
            file.write(logstr)

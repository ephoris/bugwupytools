"""
File Purpose: all custom error types defined in BUGWUpytools.tools are defined in this file.
"""
import warnings


class ImportFailedError(ImportError):
    '''error indicating that an import failed in the past, which is why a module cannot be accessed now.'''
    pass

class InputError(TypeError):
    '''error indicating something is wrong with the inputs, e.g. to a function.'''
    pass

class InputConflictError(InputError):
    '''error indicating two or more inputs provide conflicting information.
    E.g. foo(lims=None, vmin=None, vmax=None) receiving lims=(1,7), vmin=3, might raise this error,
    if the intention is for vmin and vmax to be aliases to lims[0] and lims[1].
    '''
    pass

class InputMissingError(InputError):
    '''error indicating that an input is missing AND doesn't have an appropriate default value.
    E.g. default=None; def foo(kwarg=None): if kwarg is None: kwarg=default; but foo expects non-None value.
    '''
    pass

class PatternError(ValueError):
    '''error indicating issue with some sort of pattern / matching / expectations.'''
    pass

class DataFramePatternError(PatternError):
    '''error indicating some pattern issue involving a pandas DataFrame object.'''
    pass

class WallChartsPatternError(PatternError):
    '''error indicating some pattern issue involving the wall charts.'''
    pass

class DimensionError(PatternError):
    '''error indicating issue with dimensionality, e.g. expected 2d array but got 3d.'''
    pass
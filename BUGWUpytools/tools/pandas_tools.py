"""
File Purpose: tools related to pandas package.

Notes:
    - write to excel doesn't write header & row labels from DataFrame,
        and read from excel assumes file has no header & row labels.
        This makes sense when using RangeIndex header & row labels.
"""

import os
import math

from .imports import ImportFailed
from .errors import DataFramePatternError

try:
    import pandas as pd
except ImportError:
    pd = ImportFailed('pandas')
try:
    import openpyxl
except ImportError:
    openpyxl = ImportFailed('openpyxl')
try:
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')


# default fill value for df_reshape
#DEFAULT_FILL = math.nan
DEFAULT_FILL = ''

''' --------------------- Reshape DataFrame --------------------- '''

def _df_boring_label(labels):
    '''returns whether labels are just RangeIndex or equal to a range.
    E.g. [0,1,2,3,4] --> boring. [0,4,7] --> not boring. ['hi', 'there'] --> not boring.
    labels should probably be df.index or df.columns for some DataFrame.
    '''
    if isinstance(labels, pd.RangeIndex):
        return True
    try:
        range_ = range(labels[-1]+1)
    except TypeError:
        return False
    else:
        return np.all(labels == range_)

def df_has_boring_header(df):
    '''returns whether header of df is just a RangeIndex or equal to a range.'''
    return _df_boring_label(df.columns)

def df_has_boring_labels(df):
    '''returns whether labels of df are both just RangeIndex or equal to a range.'''
    return _df_boring_label(df.index) and _df_boring_label(df.columns)

def df_reshape(df, new_shape, fill_value=DEFAULT_FILL):
    '''returns reshaped df, to new shape, filling new rows/cols with fill_value if necessary.
    df: pandas DataFrame
        index and columns must be RangeIndex (i.e. not containing data).
        Else, raise DataFramePatternError.
    new_shape: (nrows, ncols)
        result will have this many rows and this many columns.
    fill_value: object, default nan
        if nrows > df.shape[0] or ncols > df.shape[1],
            must add rows or columns to complete the operation.
            Fill all those added cells with fill_value.
        Otherwise, fill_value is ignored.
    '''
    if not _df_boring_label(df.index):
        raise DataFramePatternError('expected boring row labels, i.e. just a range, no data.')
    if not _df_boring_label(df.columns):
        raise DataFramePatternError('expected boring column labels, i.e. just a range, no data.')
    return df.reindex(index=range(new_shape[0]), columns=range(new_shape[1]), fill_value=fill_value)

def df_with_row0_as_header(df):
    '''return copy of df where header is deleted and row0 is used for header instead.'''
    df_result = df.iloc[1:, :]
    df_result.columns = df.iloc[0, :]
    return df_result

def df_with_header_as_row0(df):
    '''return copy of df where header is put into row0 and all other rows are pushed down one.
    [EFF] might not be very efficient here...
    [TODO] current implementation assumes 0 not in df.index beforehand.
    '''
    df_copy = df.iloc[:]
    header = df_copy.columns
    df_copy.columns = range(len(header))
    df_cls = type(df)
    df_header = df_cls(header, columns=[0,]).T
    result = pd.concat((df_header, df_copy), axis=0)
    return result

def df_drop_empty_cols(df, empty=''):
    '''return copy of df where all empty columns (all values == empty) are removed.'''
    any_nonempty = (df != empty).any(axis=0).values
    return df.iloc[:, any_nonempty]

def df_append_boring_col(df, fill_value=DEFAULT_FILL):
    '''return copy of df where an extra column is added and filled with blanks.'''
    return df_reshape(df, (df.shape[0], df.shape[1]+1), fill_value=fill_value)

def df_append_boring_row(df, fill_value=DEFAULT_FILL):
    '''return copy of df where an extra row is added and filled with blanks.'''
    return df_reshape(df, (df.shape[0]+1, df.shape[1]), fill_value=fill_value)


''' --------------------- difference DataFrame --------------------- '''

def df_diff(df1, df2, fill_value=DEFAULT_FILL):
    '''returns df of booleans df.iloc[i,j] telling whether df1.iloc[i,j] != df2.iloc[i,j].
    shape will be (max(r1, r2), max(c1, c2)) where df1.shape==(r1,c1), df2.shape==(r2,c2).
    fill_value is used to fill new cells if either df1 or df2 needs to be expanded; see df_reshape.
    '''
    r1, c1 = df1.shape
    r2, c2 = df2.shape
    shape = (max(r1, r2), max(c1, c2))
    if df1.shape != shape:
        df1 = df_reshape(df1, shape, fill_value)
    if df2.shape != shape:
        df2 = df_reshape(df2, shape, fill_value)
    return (df1 != df2)

def df_ndiff(df1, df2, fill_value=DEFAULT_FILL):
    '''returns number of different cells between df1 and df2.
    df1 and df2 can have different shapes, in which case:
        first expand each to the smallest shape containing both;
        fill using fill_value. see df_reshape.
    '''
    return df_diff(df1, df2, fill_value).sum().sum()

def df_ndiff_rows(df1, df2, fill_value=DEFAULT_FILL):
    '''returns number of different rows between df1 and df2'''
    return (df_diff(df1, df2, fill_value).sum('columns') != 0).sum()

def df_ndiff_rows_and_cells(df1, df2, fill_value=DEFAULT_FILL):
    '''returns (number od different rows, number of different cells) between df1 and df2.'''
    diff_rows = df_diff(df1, df2, fill_value).sum('columns')
    ndiff_rows = (diff_rows != 0).sum()
    ndiff_cells = diff_rows.sum()
    return (ndiff_rows, ndiff_cells)


''' --------------------- Excel Read/Write --------------------- '''

def _get_excel_filename(filename):
    '''returns filename with proper extension for excel.
    if filename does not end in .xlsx, append .xlsx to it.
    '''
    if not filename.endswith('.xlsx'):
        filename += '.xlsx'
    return filename

def df_write_to_excel(df, filename, sheet_name='UntitledSheet', dryrun=False,
                      header=False, index=False, **kw__to_excel):
    '''writes pandas DataFrame df to sheet_name in excel file at filename. 

    filename should end in .xlsx, else .xlsx will be appended to it.

    dryrun: bool, default False
        if dryrun, don't actually write anything! But still return the same thing.

    KW PASSED DIRECTLY TO PANDAS.DATAFRAME.TO_EXCEL:
        (named specifically to use different defaults than in pandas.to_excel())
        header: bool, default False
            whether to write header as a row in the file.
        index: bool, default False
            whether to write row-labels as a column in the file.

    returns (abspath of file, sheet_name).
    '''
    filename = _get_excel_filename(filename)
    if dryrun:
        return (os.path.abspath(filename), sheet_name)
    kw_writer = {
        'engine': 'openpyxl',
        # 'engine_kwargs': {
        #     # 'options': {   # incompatible with 'openpyxl', it seems...
        #     #     'strings_to_numbers': False,   #set True to convert strs to numbers where possible.
        #     # },
        # },
    }
    # pandas not smart enough to "use write mode if file doesn't exist", so we have to check that ourselves.
    if os.path.exists(filename):  
        kw_writemode = {
            'mode': 'a',   # append mode
            'if_sheet_exists': 'replace',   # smash old sheet if writing new one with same name.
        }  
    else:
        kw_writemode = {
            'mode': 'w',   # write new file mode. (deletes old file if it existed.. but we checked that it didn't)
        }
    kw_ = dict(header=header, index=index)
    with pd.ExcelWriter(filename, **kw_writer, **kw_writemode) as writer:   
        df.to_excel(writer, sheet_name=sheet_name, **kw_, **kw__to_excel)
    return (os.path.abspath(filename), sheet_name)

def df_read_from_excel(filename, sheet_name=None, *, custom_ext=False,
                       read_only=True, **kw__load_workbook):
    '''read sheet_name in excel file at filename, returning pandas DataFrame,
    or a dict of DataFrames with keys sheet names, if sheet_name is None.

    filename should end in .xlsx, else .xlsx will be appended to it.
    sheet_name: None or string
        string --> returns DataFrame for just this sheet.
        None --> returns dict of {name: DataFrame for sheet name}
    custom_ext: bool, default False
        if True, actually do not mess with ending of filename.

    additional kwargs are passed to openpyxl.load_workbook.

    returns pandas.DataFrame(openpyxl.load_workbook(filename)[sheet_name].values)
    '''
    if not custom_ext:
        filename = _get_excel_filename(filename)
    workbook = openpyxl.load_workbook(filename, read_only=read_only, **kw__load_workbook)
    if sheet_name is None:  # return dict of DataFrames
        result = {}
        for sheet in workbook.sheetnames:
            df = pd.DataFrame(workbook[sheet].values)
            df[df.values == None] = ''  # DataFrame compares None strangely, use '' instead.
            result[sheet] = df
        return result
    else:  # return one DataFrame
        df = pd.DataFrame(workbook[sheet_name].values)
        df[df.values == None] = ''  # DataFrame compares None strangely, use '' instead.
        return df


''' --------------------- Generic Read/Write --------------------- '''

def df_write(df, filename, *args, mode='excel', dryrun=False, **kw):
    '''write pandas DataFrame df to file at filename.
    mode: str
        mode for writing
        'excel' --> writes an excel file (.xlsx). See df_write_to_excel.
        else --> raise NotImplementedError
    dryrun: bool, default False
        if dryrun, don't write anything! But still return the same thing.
    additional args and kwargs are passed to the df_write_to_... function called based on mode.
    '''
    if mode=='excel':
        return df_write_to_excel(df, filename, *args, dryrun=dryrun, **kw)
    else:
        raise NotImplementedError(f"mode other than 'excel'. Got mode={repr(mode)}")

def df_read(filename, *args, mode='excel', **kw):
    '''reads file to return pandas DataFrame.
    mode: str
        mode for reading
        'excel' --> reads an excel file (.xlsx). See df_read_from_excel.
        else --> raise NotImplementedError
    additional args and kwargs are passed to the df_read_from_... function called based on mode.
    '''
    if mode=='excel':
        return df_read_from_excel(filename, *args, **kw)
    else:
        raise NotImplementedError(f"mode other than 'excel'. Got mode={repr(mode)}")

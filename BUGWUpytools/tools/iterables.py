"""
File Purpose: tools related to iterables
"""

import collections
import difflib

from .equality import (
    equals,
)
from .finds import (
    find,
    argmin, argmax,  # put into this namespace for convenience
)


''' --------------------- Similarities --------------------- '''

def similarity(x, y):
    '''returns some measure of the similarity of x and y, from 0 (different) to 1 (the same).
    x and y should be hashable iterables.
    [EFF] Might be slow for large x and y.
    '''
    return difflib.SequenceMatcher(None, x, y).ratio()

''' --------------------- Sort --------------------- '''

def argsort(x, reverse=False, key=lambda y: y):
    '''does an argsort using pythons builtin function: sorted'''
    return [ix[0] for ix in sorted(zip(range(len(x)), x), key=lambda ix: key(ix[1]), reverse=reverse)]

def sort_by_priorities(x, prioritize=[], de_prioritize=[], equals=equals):
    '''returns list of elements of x, reordered acoording to priorities.
    puts p in prioritize first (in order of prioritize) for any p which appear in x.
    puts p in de_prioritize last (de_prioritize[-1] goes at very end) for any p which appear in x.

    The equals key can be used to provide a custom "equals" function.
    For example, to prioritize any elements of x containing 'MEFIRST', you could do:
        sort_by_priorities(x, ['MEFIRST'], equals=lambda sp, sx: sp in sx)
    (Note that the second arg passed to equals will be the element of x.)
    '''
    start  = []
    middle = []
    end    = []
    for y in x:
        i = find(prioritize, y, default=None, equals=equals)
        if i is not None:
            start  += [(y, i)]
        else:
            j = find(de_prioritize, y, default=None, equals=equals)
            if j is not None:
                end += [(y, j)]
            else:
                middle += [y]
    # sort start and end
    start = [start[i][0] for i in argsort(start, key=lambda y_i: y_i[1])]
    end   = [end[i][0]   for i in argsort(end,   key=lambda y_i: y_i[1])]
    # return result
    return start + middle + end


''' --------------------- Misc. Shallow Iteration --------------------- '''
# iterate an iterable; accomplish some helpful task.

def counts(x, equals=equals):
    '''converts x (an iterable) to a list of tuples: (y, number of times y appears in x).'''
    result = []
    for y in x:
        for zi, [z, zcount] in enumerate(result):
            if equals(y, z):
                result[zi][1] += 1
                break
        else:  # didn't find y in result
            result.append([y, 1])
    return result

def counts_idx(x, equals=equals):
    '''converts x (an iterable) to a list of tuples: (y, list of indices where y appears in x).'''
    result = []
    for yi, y in enumerate(x):
        for zi, [z, zidx] in enumerate(result):
            if equals(y, z):
                result[zi][1].append(yi)
                break
        else:  # didn't find y in result
            result.append([y, [yi]])
    return result


''' --------------------- Categorize --------------------- '''

def dichotomize(x, condition=lambda y: y):
    '''Returns ([y for y in x if condition(y)], [y for y in x if not condition(y)])'''
    good = []
    bad  = []
    for y in x:
        (good if condition(y) else bad).append(y)
    return (good, bad)

def categorize(x, *conditions):
    '''puts each y in x into the first applicable category from conditions.
    returns tuple of lists of elements in each category. len(result) == len(conditions) + 1.
    Elements belonging to no category are placed into the final list in the result
        (or somewhere else if one of the conditions is None)

    *conditions: functions or None
        each condition is a function which accepts 1 arg. It will be passed values of y from x.
        only bool(condition(y)) will be considered here.

        use None to indicate placement for the 'default' (i.e. belonging to no category).
            at most one condition is allowed to be None.
            if None does not appear in conditions, it is equivalent to putting None at the end.

    Example:
        categorize([1,3,4,7,-2,True,8,False], lambda y: y==1, lambda y: y%2==0)
        --> ([1, True], [4, -2, 8, False], [3, 7])
        categorize([1,3,4,7,-2,True,8,False], lambda y: y==1, None, lambda y: y%2==0)
        --> ([1, True], [3, 7], [4, -2, 8, False])
    '''
    iNone = find(conditions, None, default= -1, equals=lambda v1, v2: v1 is v2)
    assert (iNone == -1) or (None not in conditions[iNone+1:]), "multiple conditions are None."
    result = tuple([] for _ in range(len(conditions)+(1 if (iNone == -1) else 0)))
    for y in x:
        for i, condition in enumerate(conditions):
            if (condition is not None) and condition(y):
                result[i].append(y)
                break
        else:  # didn't break
            result[iNone].append(y)
    return result


''' --------------------- Deep Iteration --------------------- '''
# working with iterables of iterables

def deep_iter(x, depth_first=True):
    '''iterate through all terms at all layers inside x.

    depth_first: bool, default True
        whether to go deep on terms before going wide.
    '''
    try:
        iter_x = iter(x)
    except TypeError: # x is not iterable
        return   # stop generating.
    if depth_first:
        for term in iter_x:
            yield term
            for t in deep_iter(term, False):
                yield t
    else:  # depth first
        todos = []
        for term in iter_x:
            yield term
            todos.append(term)
        for term in todos:
            for t in deep_iter(term, True):
                yield t
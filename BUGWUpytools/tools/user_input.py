"""
File Purpose: misc. tools related to asking for user input.
"""
from .errors import InputError
from .finds import find

def user_choice(options, message, Nmax=31):
    '''ask user to choose from options provided.
    Will enumerate all the options (up to Nmax),
    and prompt user to input a number or a string matching one of the options.
    If user fails to enter a valid option, prompts again, repeatedly.
    Also informs user that entering 'q' will allow them to quit.

    options: iterable
        the options to choose from.
    message: string
        message to put before listing options.
    Nmax: number, default 30
        maximum number of options to show.
        if len(options)>len(Nmax), hide all remaining options.
    '''
    if len(options) == 0:
        raise ValueError('expected at least one option for user_choice()')
    # messages
    str_options = [str(option) for option in options[:Nmax+1]]
    options_str = '\n'.join(f'  [{i:2d}] {option}' for i, option in enumerate(str_options))
    if len(options) > Nmax:
        options_str += f'\n  ... {len(options) - Nmax} additional options not shown here.'
    prompt_line = "Please select an option (enter it or its number here): "
    full_initial_prompt = '\n'.join((message, options_str, prompt_line))
    invalid_result_prompt = "Invalid input; please try again. Or enter 'q' to quit."

    # input:
    result = input(full_initial_prompt)
    def get_option(r):
        try:
            i = int(r)
        except ValueError:
            i = find(str_options, r)
            if i is None:
                raise ValueError('option not found') from None
        try:
            return options[i]
        except IndexError:
            raise ValueError('integer too large') from None

    while True:
        if result == 'q':
            raise InputError('User chose to quit instead of inputting a valid option.')
        try:
            return get_option(result)
        except ValueError:
            pass  # handled below
        result = input(invalid_result_prompt)

def user_choice_numeric(options, message, Nmax=31, secret_options=[],
                        scores=None, score_word='score', extra_prompt=''):
    '''ask user to choose from options provided.
    Will enumerate all the options (up to Nmax),
    and prompt user to input a number for one of the options.
    If user fails to enter a valid option, prompts again, repeatedly.
    Also informs user that entering 'q' will allow them to quit.

    BASIC USAGE...
    options: iterable
        the options to choose from.
    message: string
        message to put before listing options.
    Nmax: number, default 30
        maximum number of options to show.
        if len(options)>len(Nmax), hide all remaining options.
    secret_options: iterable, default empty list
        if user inputs any of these options, return it immediately.
        However this function does not list the secret_options directly.
        (Consider using message to describe them.)

    EXTRA BEHAVIORS...
    scores: None or iterable with same length as options
        if provided, the options' scores; will be printed in-line to better inform the user.
        recommended to enter scores as a list of formatted strings.
    score_word: str, default 'score'
        if score is provided, format lines as: [NN] (score_word=score) option
    numeric_only: bool, default False
        whether to force user to enter numeric option.
        if False, accept full string option, or option number. Returns full string option.
        if True, only accept option number. Returns option number.
    extra_prompt: ''
        goes after 'Please select an option by number' and before ': '.
    '''
    if len(options) == 0:
        raise ValueError('expected at least one option for user_choice()')
    # messages
    str_options = [str(option) for option in options[:Nmax+1]]
    if scores is None:
        options_str = '\n'.join(f'  [{i:2d}] {option}' for i, option in enumerate(str_options))
    else:
        options_str = '\n'.join(f'  [{i:2d}] ({score_word}={score}) {option}'
                                for i, (option, score) in enumerate(zip(str_options, scores)))
    if len(options) > Nmax:
        options_str += f'\n  ... {len(options) - Nmax} additional options not shown here.'
    prompt_line = f"Please select an option by number{extra_prompt}: "
    full_initial_prompt = '\n'.join((message, options_str, prompt_line))
    invalid_result_prompt = "Invalid input; please try again. Or enter 'q' to quit."

    # input:
    result = input(full_initial_prompt)
    while True:
        if result == 'q':
            raise InputError('User chose to quit instead of inputting a valid option.')
        if result in secret_options:
            return result
        try:
            i = int(result)
        except ValueError:
            pass  # not an int -- handled below.
        else:
            if i < len(options):
                return i
            # otherwise, int too large -- handled below.
        result = input(invalid_result_prompt)
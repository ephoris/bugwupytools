"""
File Purpose: reloading modules / packages

Example:
    import BUGWUpytools as bp
    bp = bp.reload()

or, to reload a different package:
    import BUGWUpytools as bp
    import other_package as other_package
    other_package = bp.reload(other_package)
"""
import importlib
import inspect
import sys
import warnings

from .errors import ImportFailedError


''' --------------------- reloading --------------------- '''

def enable_reload(package='BUGWUpytools'):
    '''smashes the import cache for the provided package.
    All modules starting with this name will be removed from sys.modules.
    This does not actually reload any modules.
    However, it means the next time import is called for those modules, they will be reloaded.
    returns tuple listing of all names of affected modules.

    package: str or module
        the package for which to enable reload. if module, use package.__name__.
    '''
    if inspect.ismodule(package):
        package = package.__name__
    l = tuple(key for key in sys.modules.keys() if key.startswith(package))
    for key in l:
        del sys.modules[key]
    return l

def reload(package='BUGWUpytools', return_affected=False):
    '''reloads the provided package.
    Equivalent to enable_reload(package); import package.

    returns the reloaded package.
    if return_affected also return a list of all affected package names.

    CAUTION: this reloads package but doesn't do it "in-place" (i.e. doesn't change the package variable).
    To use this method properly, you should assign the package to the result:
        import BUGWUpytools as bp
        bp = bp.reload()

        # or, to reload a different package
        import mypackage as myp
        myp = bp.reload(myp)

        # (If you fail to assign the result, what will happen?)
        myp1 = bp.reload(myp0)
        myp1 is myp0   # --> False, because myp0 points to the pre-reload version of the package.

    package: str or module
        the package to reload. if module, use package.__name__.
    '''
    if inspect.ismodule(package):
        package = package.__name__
    affected = enable_reload(package)
    result = importlib.import_module(package)
    return (result, affected) if return_affected else result


''' --------------------- import failure handling --------------------- '''

class ImportFailed():
    '''set modules which fail to import to be instances of this class;
    initialize with modulename, additional_error_message.
    when attempting to access any attribute of the ImportFailed object,
        raises ImportFailedError('. '.join(modulename, additional_error_message)).
    Also, make warning immediately when initialized.

    Example:
    try:
        import zarr
    except ImportError:
        zarr = ImportFailed('zarr', 'This module is required for compressing data.')

    zarr.load(...)   # << attempt to use zarr
    # if zarr was imported successfully, it will work fine.
    # if zarr failed to import, this error will be raised:
    >>> ImportFailedError: zarr. This module is required for compressing data.
    '''
    def __init__(self, modulename, additional_error_message=''):
        self.modulename = modulename
        self.additional_error_message = additional_error_message
        warnings.warn(f'Failed to import module {modulename}.{additional_error_message}')

    def __getattr__(self, attr):
        str_add = str(self.additional_error_message)
        if len(str_add) > 0:
            str_add = '. ' + str_add
        raise ImportFailedError(self.modulename + str_add)

    def __repr__(self):
        return f'ImportFailed({self.modulename!r})'

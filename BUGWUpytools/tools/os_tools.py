"""
File Purpose: misc. tools related to file management / os
"""
import os
from .user_input import user_choice

class InDirectory:
    '''context manager for remembering directory.
    upon enter, cd to directory
        if directory is None, use os.curdir by default, i.e. no change in directory.
    upon exit, original working directory will be restored.
    '''

    def __init__(self, directory=None):
        self.directory = directory

    def __enter__(self):
        self.cwd = os.path.abspath(os.getcwd())
        os.chdir(self.directory)

    def __exit__(self, _exc_type, _exc_value, _traceback):
        os.chdir(self.cwd)


def _file_candidates(startswith=None, dir='.', *, matches=None):
    '''returns list of files in dir stating with startswith or matching regular expression matches.
    regular expression matching not yet implemented.
    if startswith is None, list all files in dir.

    result will be sorted alphabetically.
    '''
    if matches is not None:
        raise NotImplementedError("non-None 'matches' kwarg.")
    files = os.listdir(dir)
    if startswith is None:
        return files
    else:
        return sorted([file for file in files if file.startswith(startswith)])

def choose_file(startswith=None, dir='.', *,  matches=None, choice_method='user'):
    '''chooses a file in dir starting with startswith, or matching regular expression matches.
    matches has not yet been implemented.
    choice_method: 'user', 'first', or 'last'.
        how to choose the file if there are multiple candidates matching the criteria.
        'user' --> ask for user input. This is the default.
        'first' --> use the first file (sorted alphabetically)
        'last' --> use the last file (sorted alphabetically)
    '''
    candidates = _file_candidates(startswith=startswith, dir=dir, matches=matches)
    absdir = os.path.abspath(dir)
    if len(candidates) == 0:
        errmsg = f'file starting with {repr(startswith)} in directory {repr(absdir)}'
        raise FileNotFoundError(errmsg)
    elif len(candidates) == 1:
        return candidates[0]
    # else, len(candidates) > 1
    if choice_method == 'user':
        input_str = 'Found multiple files matching criteria provided. Which one did you mean?'
        input_str += f'\nIn directory {repr(absdir)}, options are:'
        return user_choice(candidates, input_str)
    elif choice_method == 'first':
        return candidates[0]
    elif choice_method == 'last':
        return candidates[-1]
    else:
        errmsg = f"choice_method invalid, expected 'user', 'first', or 'last', but got {repr(choice_method)}"
        raise ValueError(errmsg)

"""
File Purpose: misc. tools for arrays, e.g. numpy arrays
"""

from .imports import ImportFailed

try:
    import numpy as np
except ImportError:
    np = ImportFailed('numpy')

from .errors import PatternError


def first_i_where_True(arr):
    '''returns first index such that arr[i]==True.
    raises IndexError if this is not possible.
    raises PatternError if arr is not 1d.
    '''
    nparr = np.asanyarray(arr)
    if nparr.ndim != 1:
        raise PatternError(f'Expected 1d array but got ndim={nparr.ndim}')
    idx_where_True = np.where(arr)[0]
    try:
        return idx_where_True[0]
    except IndexError:
        raise IndexError(f'True not found in array with length={len(nparr)}') from None

def list_of_lists_to_array(x, fill_value=None, dtype=object, **kw_array):
    '''converts list of lists to a numpy array,
    making all lists the same length first if necessary (fill with fill_value).
    additional kwargs are passed to np.array()
    '''
    length = max(len(row) for row in x)
    return np.array(tuple(row + [fill_value]*(length - len(row)) for row in x), dtype=object, **kw_array)

def array_to_list_of_lists(x):
    '''return list of lists, given numpy array.
    pygsheets sometimes assumes things are lists and not arrays, making this necessary.
    '''
    return [list(row) for row in np.asarray(x)]

def df_to_list_of_lists(x):
    '''return list of lists of strings, given pandas DataFrame.
    pygsheets sometimes assumes things are lists and not arrays, making this necessary.
    '''
    return [[str(val) for val in row] for i, row in x.iterrows()]

def list_shape(x):
    '''returns shape of list of lists.
    Assumes (but does not check) that x could be converted to a numpy array.
    (I.e. all lists within a list have the same length.)
    note: explicitly checks isinstance(obj, list).
    '''
    shape = []
    y = x
    while isinstance(y, list):
        L = len(y)
        shape.append(L)
        if L == 0:
            break
        y = y[0]
    return tuple(shape)

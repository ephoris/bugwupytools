"""
Package Purpose: Miscellaneous quality-of-life functions.
This package is intended to provide functions which:
- are helpful for solving specific, small problems
- could be useful in other projects as well
    i.e., these should not depend on other parts of BUGWUpytools.

This file:
Imports the main important objects throughout this subpackage.
"""
import warnings

from .arrays import (
    first_i_where_True,
    list_of_lists_to_array, array_to_list_of_lists,
    df_to_list_of_lists,
    list_shape,
)
from .charts import (
    Chart,
    CellIndex, CellRangeIndex,
    colstr_to_index, index_to_colstr,
    rowstr_to_index, index_to_rowstr,
    cell_str_split,
    rangestr_to_corners, corners_to_rangestr,
    cellstr_to_index, index_to_cellstr,
    rangestr_to_index, index_to_rangestr,
)
from .display import (
    replace_with_clickable_urls, display_with_clickable_urls, clickable_urls,
    Logger,
)
from .equality import (
    equals, list_equals, dict_equals, equal_sets,
)
from .errors import (
    ImportFailedError,
    InputError, InputConflictError, InputMissingError,
    PatternError, DataFramePatternError, WallChartsPatternError,
    DimensionError,
)
from .finds import (
    find,
    argmin, argmax,
)
from .imports import (
    enable_reload, reload, ImportFailed,
)
from .iterables import (
    similarity,
    argsort, sort_by_priorities,
    counts, counts_idx,
    dichotomize, categorize,
    deep_iter,
)
from .oop_misc import (
    alias, alias_to_result_of, alias_in,
    caching_attr_simple_if,
    maintain_attrs, MaintainingAttrs,
)
from .os_tools import (
    InDirectory, choose_file,
)
from .pandas_tools import (
    _df_boring_label, df_has_boring_labels, df_has_boring_header,
    df_reshape, df_append_boring_col, df_append_boring_row,
    df_with_row0_as_header, df_with_header_as_row0,
    df_drop_empty_cols,
    df_diff, df_ndiff, df_ndiff_rows, df_ndiff_rows_and_cells,
    df_write, df_read,
    df_write_to_excel, df_read_from_excel,
    _get_excel_filename,
)
# << pygsheets_tools import is below others since it may fail if pygsheets not installed.
from .pytools import (
    format_docstring,
    value_from_aliases,
    assert_values_provided,
    printsource,
    inputs_as_dict, _inputs_as_dict__maker,
)
from .sentinels import (
    NO_VALUE, RESULT_MISSING,
)
from .timing import (
    datetime_now, strtime, strtime_now, strptime,
)
from .user_input import (
    user_choice, user_choice_numeric,
)

try:
    import pygsheets
except ImportError:
    pygsheets = ImportFailed('pygsheets')
    warnings.warn('Failed to import pygsheets; contents of pygsheets_tools will be unavailable.')
else:
    from .pygsheets_tools import (
        open_spreadsheet, open_sheets_doc,
        SheetsDoc, SheetsChart,
        hex_to_rgb_dict, fontsize_pixels,
    )
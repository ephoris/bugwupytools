"""
File Purpose: some default values for wall chart subpackage.
"""

DEFAULT_SAVEDIR = 'wall_chart_backups'
DEFAULT_META_NAME = 'WallChartMeta'
THESIS_NAME = 'wallcharts_backup'

DASHBOARD_NAME = 'dashboard'
DASHBOARD_BUFFER = (10, 5)

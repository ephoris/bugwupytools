"""
File Purpose: ThesisPersonInfo, ThesisPerson
"""
import warnings
import weakref

import numpy as np

from ..tools import (
    InputMissingError,
    assert_values_provided,
    alias,
    NO_VALUE,
)

class ThesisPersonInfo():
    '''info about this person and how they relate to the thesis.

    thesis: WallThesisData related to this info.
    doc: name of the document.
    sheet: name of the sheet, within that document.
    row: row index where person appears.
        (0 for first piece of data; as if there were no header.
         compatible with chart.column(key), but for direct indexing use chart[row + 1].)
    first_name: 'First name' column value at thesis[doc][sheet][row+1]
        if NO_VALUE, read the value from thesis during __init__ here.
    last_name:  'Last name'  column value at thesis[doc][sheet][row+1]
        if NO_VALUE, read the value from thesis during __init__ here.
    url: url corresponding to this doc, if known.
        if NO_VALUE, read the value from thesis during __init__ here.
    '''
    def __init__(self, thesis, doc, sheet, row, first_name=NO_VALUE, last_name=NO_VALUE, url=NO_VALUE):
        self.thesis = thesis
        self.doc = doc
        self.sheet = sheet
        self.row = row
        self.first_name = first_name
        self.last_name = last_name
        self.url = url
        self._read_name_if_NO_VALUE()
        self._read_url_if_NO_VALUE()

    def _read_name_if_NO_VALUE(self):
        '''if self.first_name or self.last_name are NO_VALUE, read them from thesis.'''
        first_None = self.first_name is NO_VALUE
        last_None = self.last_name is NO_VALUE
        if first_None or last_None:
            chart = self.thesis[self.doc][self.sheet]
            first, last = chart.columns('First name', 'Last name')[self.row]
            if first_None: self.first_name = first
            if last_None: self.last_name = last

    def _read_url_if_NO_VALUE(self):
        '''if self.url is NO_VALUE, read it from thesis.'''
        if self.url is NO_VALUE:
            self.url = self.thesis.get_url(self.doc)

    # # # CONVENIENT PROPERTIES # # #
    name = property(lambda self: (self.first_name, self.last_name), doc='''(first_name, last_name) from self.''')
    caddress = property(lambda self: (self.doc, self.sheet), doc='''(doc, sheet) from self.''')
    raddress = property(lambda self: (self.doc, self.sheet, self.row), doc='''(doc, sheet, row) from self.''')
    uaddress = property(lambda self: (self.doc, self.sheet, self.row, self.url), doc='''(doc, sheet, row, url) from self.''')

    # # # COMPARISONS # # #
    def same_name_and_sheet(self, x):
        '''tells whether self and x have the equal 'doc', 'sheet', 'first_name', and 'last_name'.'''
        return self.doc==x.doc and self.sheet==x.sheet and \
                self.first_name==x.first_name and self.last_name == x.last_name


    # # # ITERATING # # #
    @property
    def tuple_(self):
        '''(docname, sheetname, row_index, first_name, last_name) from self.'''
        return (self.doc, self.sheet, self.row, self.first_name, self.last_name)

    def __iter__(self):
        '''iterates through (docname, sheetname, row_index, first_name, last_name)'''
        return iter(self.tuple_)

    def __getitem__(self, i):
        '''returns self.tuple_[i]'''
        return self.tuple_[i]

    # # # WEAKREF # # #
    @property
    def thesis(self):
        '''WallThesisData related to this info. (Internally handled as a weakref.)'''
        result = self._thesis()
        if result is None:
            raise ValueError('original thesis object no longer exists! Got None from weakref...')
        else:
            return result
    @thesis.setter
    def thesis(self, value):
        self._thesis = (lambda: None) if value is None else weakref.ref(value)

    # # # GET STUFF # # #
    def get_row(self, *, labeled=True):
        '''returns the row from thesis corresponding to self.
        labeled: bool, default True
            True --> return a Chart with just "header row" and this row
            False --> return an array with just this row

        Equivalent to self.thesis.get_row(*self, labeled=labeled)
        '''
        return self.thesis.get_row(*self, labeled=labeled)

    def get_person(self):
        '''returns the ThesisPerson corresponding to self.'''
        return ThesisPerson(self)

    get = alias('get_person')

    # # # DISPLAY # # #
    def __repr__(self):
        '''repr of self.'''
        contents = ', '.join(self._repr_contents())
        return f'{type(self).__name__}({contents})'

    def _repr_contents(self):
        '''list of contents to go into ThesisPersonInfo() during repr of self.'''
        return [repr(self.doc), repr(self.sheet), repr(self.row),
                repr(self.first_name), repr(self.last_name)]


class ThesisPerson(dict):
    '''data about this person. Also person.info refers to the relevant ThesisPersonInfo.

    info_or_thesis: ThesisPersonInfo or WallThesisData
        if ThesisPersonInfo, no need to provide doc, sheet, row.
        else, need to provide doc, sheet, row.
    doc: None or doc name in thesis where this person appears.
    sheet: None or sheet name in thesis[doc] where this person appears.
    row: None or row index in thesis[doc][sheet] where this person appears.
        (0 for first piece of data; as if there were no header.)

    self[key] works like usual but ALSO if key in self.SPECIAL_KEYS, get self.{key} instead.
        SPECIAL_KEYS include:
            'doc', 'sheet', 'row', 'first_name', 'last_name', 'url',
            'caddress', 'raddress', 'uaddress', 'name'
        e.g. self['sheet'] gets self.sheet instead of super().__getitem__('sheet').
    '''
    ThesisPersonInfo_type = ThesisPersonInfo

    def __init__(self, info_or_thesis, doc=None, sheet=None, row=None):
        if isinstance(info_or_thesis, self.ThesisPersonInfo_type):
            info = info_or_thesis
        else:
            thesis = info_or_thesis
            assert_values_provided(doc=doc, sheet=sheet, row=row)
            info = self.ThesisPersonInfo_type(thesis, doc, sheet, row)
        self.info = info
        self.load_data()
        self._special_keys_check()

    def load_data(self):
        '''puts values from thesis for this person into self (access like a dict).
        Also sets self.header to the header of the corresponding chart.
        [TODO] handle duplicate header labels
        '''
        chart = self.info.get_row(labeled=True)
        self.header = chart.header
        row = chart.get_row(1)
        for i, key in enumerate(self.header):
            self[key] = row[i]

    # # # LOOKUP # # #
    def values(self, *keys, asarray=False):
        '''returns list [self[key] for key in keys], OR super().values if len(keys)==0.
        if asarray, convert result to numpy array.
        '''
        if len(keys) == 0:
            result = super().values()
        else:
            result = [self[key] for key in keys]
        return np.array(result) if asarray else result

    # # # CONVENIENT PROPERTIES & SPECIAL LOOKUP # # #
    thesis   = property(lambda self: self.info.thesis,   doc='''alias to self.info.thesis''')
    doc      = property(lambda self: self.info.doc,      doc='''alias to self.info.doc''')
    sheet    = property(lambda self: self.info.sheet,    doc='''alias to self.info.sheet''')
    row      = property(lambda self: self.info.row,      doc='''alias to self.info.row''')
    url      = property(lambda self: self.info.url,      doc='''alias to self.info.url''')
    caddress = property(lambda self: self.info.caddress, doc='''alias to self.info.caddress''')
    raddress = property(lambda self: self.info.raddress, doc='''alias to self.info.raddress''')
    uaddress = property(lambda self: self.info.uaddress, doc='''alias to self.info.uaddress''')

    first_name = property(lambda self: self.info.first_name, doc='''alias to self.info.first_name''')
    last_name  = property(lambda self: self.info.last_name,  doc='''alias to self.info.last_name''')
    name       = property(lambda self: self.info.name,       doc='''alias to self.info.name''')

    SPECIAL_KEYS = ('doc', 'sheet', 'row', 'first_name', 'last_name', 'url',
                    'caddress', 'raddress', 'uaddress', 'name')

    def _special_keys_check(self):
        '''checks that self.keys() does not contain any of the special keys.'''
        for key in self.SPECIAL_KEYS:
            if key in self:
                warnmsg = (f'key {repr(key)} is special, but appears in self.keys(). '
                           f'Attempting self[{repr(key)}] will return self.{key} instead. '
                           f'To get the data instead of self.{key}, use self["{key}_"] instead.')
                warnings.warn(warnmsg)
                return False

    def __getitem__(self, key):
        '''returns self[key]. if key in self.SPECIAL_KEYS, get attribute from self, instead.
        if key is special key plus ending in '_', get super()[key].
        E.g., self['nonspecial'] --> super()['nonspecial']
              self['doc'] --> self.doc
              self['doc_'] --> super()['doc']
        '''
        if key in self.SPECIAL_KEYS:
            return getattr(self, key)
        elif key.endswith('_') and key[:-1] in self.SPECIAL_KEYS:
            return super().__getitem__(key[:-1])
        else:
            return super().__getitem__(key)

    def getvals(self, *keys, expand_special=True, asarray=False, **kw):
        '''returns list of [self[key] for key in keys], allowing for special keys.
        special keys are:
            'doc', 'sheet', 'row', 'first_name', 'last_name', 'url'
                --> get corresponding attribute from self.
            'caddress', 'raddress', 'uaddress', 'name'
                --> get corresponding attribute from self; expand if expand_special.
        expand_special: bool, default True
            whether to expand special keys 'caddress', 'raddress', 'uaddress', 'name'.
            "expand" means put multiple values for each key.
            E.g. if True, self.getvals('name') == self.getvals('first_name', 'last_name')
        asarray: bool, default False
            whether to convert result to numpy array.
        
        default: any value; optional kwarg
            if provided, and can't find key, use default value.
            e.g. getvals(..., default='MISSING VALUE')
        '''
        if len(keys) == 0:
            raise InputMissingError(f'{type(self).__name__}.getvals() requires at least 1 key.')
        default_provided = 'default' in kw
        if default_provided:
            default = kw['default']
        result = []
        for key in keys:
            if key in ('doc', 'sheet', 'row', 'first_name', 'last_name', 'url'):
                result += [getattr(self, key)]  # list addition
            elif key in ('caddress', 'raddress', 'uaddress', 'name'):
                val = getattr(self, key)
                vals = list(val) if expand_special else [val]
                result += vals  # list addition
            else:
                if default_provided:
                    val = self.get(key, default)
                else:
                    val = self[key]
                result += [val]
        return np.array(result) if asarray else result

    # # # DISPLAY # # #
    def __repr__(self):
        '''repr of self.'''
        super_repr = super().__repr__()[1:-1]  # dict repr without '{' and '}'.
        return f"{type(self).__name__}{{'raddress': {self.raddress}, {super_repr}}}"


class MiniPerson(ThesisPerson):
    '''person from only (doc, sheet, row, first_name, last_name), and possibly other kwargs.

    Especially useful with ThesisData.set_column_val;
        can create a list of MiniPerson objects from data.
        E.g. make excel doc with 'doc', 'sheet', 'row', 'first_name', 'last_name',
            give to someone else to process / add whatever data they want,
            read their results back into python with chart = Chart.from_excel()['UntitledSheet'],
            assert all people still match current people, e.g.:
                for person, row in zip(td.people, chart.values):  # td = ThesisData(...)
                    mini = MiniPerson(*row[:5])
                    assert mini.raddress == tuple(person.getvals('raddress'))
                    assert mini.name == person.name
            use ThesisData.set_column_val to set new data. E.g. if new column is 'BU email':
            mini_people = [MiniPerson(*row[:5], **{'BU email':row[5]}) for row in chart.values]
            td.set_column_val('BU email', mini_people)
    '''
    def __init__(self, doc, sheet, row, first_name=None, last_name=None, **kw):
        self.info = ThesisPersonInfo(None, doc, sheet, int(row), first_name=first_name, last_name=last_name, url=None)
        # ^ use None instead of NO_VALUE for name & url, so that we don't try read it from thesis.
        if first_name is not None: self['First name'] = first_name
        if last_name is not None: self['Last name'] = last_name
        self.update(kw)


class PersonList(list):
    '''list of ThesisPerson objects, but lookup can go by strings instead.
    self[key] will be [e[key] for e in self] if key is string. Otherwise use list[key]
    
    to_array: bool
        if True, convert self[key] to numpy array if it would otherwise be a list.
        E.g. self[5] --> single value
            self[:5] --> numpy array if to_array, else list
            self['first_name'] --> numpy array if to_array, else list
    '''
    def __init__(self, *args_super, to_array=True, **kw_super):
        super().__init__(*args_super, **kw_super)
        self.to_array = to_array

    def __getitem__(self, key):
        '''returns self[key]. if key is string, use [e[key] for e in self]; else, use list[key].'''
        if isinstance(key, str):
            result = type(self)([e[key] for e in self])
        else:
            result = super().__getitem__(key)
        if isinstance(result, list) and self.to_array:
            result = np.array(result)
        return result

    def keys(self):
        '''returns list of keys shared by all people in self.'''
        result = set(self[0].keys())
        for person in self[1:]:
            result &= set(person.keys())
        return list(result)

    def __repr__(self):
        '''repr of self.'''
        list_repr = super().__repr__()[1:-1]  # list repr without '[' and ']'.
        return f'{type(self).__name__}({list_repr})'
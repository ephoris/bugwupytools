"""
Package Purpose: manipulate wall charts.

This file: imports the main important things in this subpackage.
"""

from . import wall_chart_defaults

# basic objects; wall chart / data managers:
from .wall_chart import WallChart
from .wall_chart_data import WallChartData
from .wall_doc import WallDoc, open_walldoc
from .wall_doc_data import WallDocData
from .wall_metadata import WallMetadata
from .wall_thesis import WallThesis
from .wall_thesis_data import WallThesisData

# dashboard
from .dashboard_builder import DashboardPrefix, DashboardBuilder
from .dashboard_formula_builder import DashboardFormulaBuilder

# other objects...
from .header_mapping import (
    HeaderMapping,
)
from .people import (
    ThesisPersonInfo, ThesisPerson,
    MiniPerson,
    PersonList,
)

# building wall chart in standard format
from . import build_wall_chart
from .build_wall_chart import rebuild_wall_sheet

from . import build_dashboard
from .build_dashboard import rebuild_dashboard

from . import build_wall_doc
from .build_wall_doc import rebuild_wall_doc
"""
File Purpose: WallChartData class
"""
import os

try:
    import numpy as np
except ImportError:
    from ..tools import ImportFailed
    np = ImportFailed('numpy')

from .header_mapping import HeaderMapping
from ..tools import (
    Chart,
    datetime_now, strtime, strptime,
    InDirectory, choose_file,
    format_docstring,
)
from . import wall_chart_defaults as defaults


_from_excel_paramdocs_nofile = \
    '''dir: None or value
            None --> use wall_chart_defaults.DEFAULT_SAVEDIR
            if still None --> use current directory
            look in this directory (only) for filename.
            Note: to ignore dir, use an abspath for filename.
        choice_method: 'user', 'first', or 'last'.
            how to choose the file if there are multiple candidates starting with filename.
            'user' --> ask for user input. This is the default.
            'first' --> use the first file (sorted alphabetically)
            'last' --> use the last file (sorted alphabetically)'''

_from_excel_paramdocs = \
    f'''filename: str or None
            start of filename, or full filename.
            start of filename --> choose a file starting with this string.
                (may prompt user if there are multiple options; see choice_method.)
            full filename --> should end in .xlsx, else .xlsx will be appended to it.
        {_from_excel_paramdocs_nofile}'''
        

class WallChartData(Chart):
    '''Data from a wall chart. Not connected to a google doc.
    Provides some "standard" methods for manipulating the data.

    self.meta stores info:
        doc_name: name of the google sheets doc this wall chart data came from.
        sheet_name: name of the worksheet this wall chart data came from.
        t_read: datetime when the wall chart data was read from the google doc.
        _source: None, 'gsheet', or 'excel'. Where this data was read from.
    '''
    @classmethod
    def _meta_like(cls, *input_arrays):
        '''returns meta combined from all inputs with same type (or sublcass) as cls,
        keeping leftmost-input values in case of conflict.
        Also sets values for keys '_source' and 't_read' to None
            to indicate result doesn't come directly from a source.
        '''
        meta_out = super()._meta_like(*input_arrays)
        meta_out['_source'] = None
        meta_out['t_read'] = None
        return meta_out

    def _on_data_update(self):
        '''what to do when data in self is updated.
        sets self.meta['_source'] = self.meta['t_read'] = None.
        '''
        self.meta['_source'] = None
        self.meta['t_read'] = None

    @classmethod
    def from_wallchart(cls, wallchart, **kw__get_array):
        '''initialize self from WallChart object; get df and meta.'''
        arr = wallchart.get_array(**kw__get_array)
        now = datetime_now()
        meta = dict(
            doc_name    = wallchart.spreadsheet.title,
            sheet_name  = wallchart.title,
            t_read      = now,
            _source     = 'gsheet',
        )
        return cls(arr, meta=meta)

    doc_name   = property(lambda self: self.meta['doc_name'],   doc='''alias to self.meta['doc_name']''')
    sheet_name = property(lambda self: self.meta['sheet_name'], doc='''alias to self.meta['sheet_name']''')
    t_read     = property(lambda self: self.meta['t_read'],     doc='''alias to self.meta['t_read']''')
    _source    = property(lambda self: self.meta['_source'],    doc='''alias to self.meta['_source']''')

    caddress   = property(lambda self: (self.doc_name, self.sheet_name), doc='''(doc_name, sheet_name) from self''')

    # # # DISPLAY # # #
    def _repr_contents(self):
        '''return list of contents to go into repr of self.'''
        contents = super()._repr_contents()
        doc_name, sheet_name, t_read = self._get_param_strings(pure=True)
        pstring = f'doc={repr(doc_name)}, sheet={repr(sheet_name)}, t_read={repr(t_read)}'
        contents.append(f'from {pstring}')
        return contents

    def _get_doc_name_string(self):
        '''returns string for self.doc_name.'''
        doc = self.doc_name
        return str(doc) if doc is not None else 'DOCNAME_UNKNOWN'

    def _get_sheet_name_string(self):
        '''returns string for self.sheet_name.'''
        sheet = self.sheet_name
        return str(sheet) if sheet is not None else 'SHEETNAME_UNKNOWN'

    def _get_t_read_string_pure(self):
        '''returns formatted string for self.t_read.'''
        t = self.t_read
        return strtime(t) if t is not None else 'READTIME_UNKNOWN'

    def _get_t_read_string(self):
        '''returns formatted string for self.t_read; either '' or str with leading '__'. '''
        t = self.t_read
        return '__'+strtime(t) if t is not None else ''

    def _get_param_strings(self, pure=False):
        '''returns (doc_name, sheet_name, t_read) as strings using the _get_<param>_string methods.'''
        doc_name = self._get_doc_name_string()
        sheet_name = self._get_sheet_name_string()
        t_read = self._get_t_read_string_pure() if pure else self._get_t_read_string()
        return (doc_name, sheet_name, t_read)

    # # # FILENAME STRINGS # # #
    def _get_default_dst_excel(self):
        '''returns default (filename, sheet name) for saving self.'''
        doc_name, sheet_name, t_read = self._get_param_strings()
        return (f'{doc_name}{t_read}.xlsx', sheet_name)

    @staticmethod
    def _extract_meta_from_filename(filename):
        '''returns dict with keys doc_name, t_read, based on filename.
        Assumes if '__' in filename, the final '__' splits between name and t_read.
        '''
        noextname, ext = os.path.splitext(filename)
        if (ext != '.xlsx'):
            raise ValueError(f"expected filename ending in '.xlsx', but got {repr(filename)}")
        basename = os.path.basename(noextname)
        splitted = basename.rsplit('__', 1)
        result = {'doc_name': splitted[0]}
        if len(splitted) == 1:
            result['t_read'] = None
        else:
            t_read = splitted[1]
            t_read = strptime(splitted[1])  # converts str to datetime
            result['t_read'] = t_read
        return result

    # # # EXCEL READ / WRITE # # #
    def to_excel(self, dst=None, sheet_name=None, dir=None, dryrun=False, **kw__write):
        '''saves wall chart to local machine at dst.
        if dst not provided, use self._get_default_dst_excel().
        if dir provided, save to that directory, making it if necessary.
        if dir not provided, try DEFAULT_SAVEDIR too.

        dryrun: bool, default False
            if dryrun, don't actually write to excel! But still return the same thing.

        returns (abspath to saved file, sheet name)
        '''
        # filename and sheet name
        default_dst, default_sheet_name = self._get_default_dst_excel()
        if dst is None:
            dst = default_dst
        if sheet_name is None:
            sheet_name = default_sheet_name
        if dryrun:
            return super().to_excel(dst, sheet_name=sheet_name, dryrun=True, **kw__write)
        # directory
        if dir is None:
            dir = defaults.DEFAULT_SAVEDIR
        if dir is not None:
            os.makedirs(dir, exist_ok=True)
        with InDirectory(dir):  # cd dir. Upon exiting this block: cd previous cwd.
            # write to excel:
            return super().to_excel(dst, sheet_name=sheet_name, dryrun=False, **kw__write)

    @classmethod
    @format_docstring(paramdocs=_from_excel_paramdocs)
    def from_excel(cls, filename, sheet_name=None, *, dir=None, choice_method='user', **kw__read):
        '''read sheet_name in excel file at filename, returning a WallChartData object
        or a dict of Charts with keys sheet names, if sheet_name is None.
        
        {paramdocs}

        for more docs, see Chart.from_excel.
        '''
        if dir is None:
            dir = defaults.DEFAULT_SAVEDIR
        with InDirectory(dir):
            filename = choose_file(startswith=filename, choice_method=choice_method)
            meta = cls._extract_meta_from_filename(filename)
            meta.update(sheet_name=sheet_name, _source='excel')
            result = super().from_excel(filename, sheet_name=sheet_name, meta=meta, **kw__read)
        if isinstance(result, dict):  # << bookkeeping for sheet names in case of multiple sheets
            for key, val in result.items():
                val.meta['sheet_name'] = key
        return result

    # # # EDIT VALUES # # #
    def __setitem__(self, index, value):
        '''does super().__setitem__(index, value). Then calls self._on_data_update().'''
        super().__setitem__(index, value)
        self._on_data_update()

    # # # INSPECT # # #
    def header_mapping(self, other_wcd, calculate_matches=True, **kw):
        '''returns HeaderMapping hm with self.header as header1, and other_wcd.header as header2.
        if calculate_matches, calculate the matches now.

        Given i for self, use j=hm.map1to2[i] to get j with corresponding header of other_wcd.
        Given j for other_wcd, use i=hm.map2to1[j] to get i with corresponding header of self.
        '''
        return HeaderMapping(self.header, other_wcd.header, calculate_matches=calculate_matches, **kw)

    @property
    def n_people(self):
        '''number of people in this chart.'''
        result = 0
        for irow, (first, last) in enumerate(self.columns('First name', 'Last name')):
            if first != '' or last != '':
                result += 1
        return result

    def contains_person(self, *, BU_email=None, personal_email=None):
        '''whether this chart contains this person, as specified by some unique identifier(s).'''
        if all(val is None for val in (BU_email, personal_email)):
            raise ValueError("Expected one of ['BU email', 'personal email'], but got None.")
        if BU_email is not None and personal_email is not None:
            raise ValueError("Expected exactly one of ['BU email', 'personal email'], but got both.")
        if BU_email is not None:
            return self.has_value_in_column('BU email', BU_email)
        if personal_email is not None:
            return self.has_value_in_column('Personal email', personal_email)

    # # # COMPARE # # #
    def add_people(self, wcd, *, person_cols=['First name', 'Last name'],
                   tracker_col=None, _tracker_True=True, **kw__header_mapping):
        '''adds people from other WallChartData wcd into self.df.
        Does not alter any rows with people from wcd.
        Does not add any people which are already in self.

        person_cols: list of strings
            column names to use for person definition.
            all indicated cells must match exactly for it to be considered the same person.
        tracker_col: None or str
            if provided, add a column to self to tell which rows were added via this operation.
            Use added_col as the label for this column (the label will go into row 0).
        _tracker_True: bool, default True
            value to put into tracker_col for rows added by this operation.
            (other rows, except row 0, will have (not _tracker_True) instead.)
        '''
        if tracker_col is not None:
            self = self.append_empty_cols(1)  # append 1 empty col
            self[0, -1] = tracker_col  # header for the tracker column
            self[1:, -1] = not _tracker_True  # didn't add any of the things that are already here.
        hm = self.header_mapping(wcd, **kw__header_mapping)
        index_pcols_self = [self.column_index_from_label(pcol) for pcol in person_cols]
        index_pcols_wcd  = [wcd.column_index_from_label(pcol) for pcol in person_cols]
        people_in_self = self[1:, index_pcols_self]
        people_in_wcd  = wcd[1:, index_pcols_wcd]

        for iperson, person in enumerate(np.asarray(people_in_wcd)):
            index_row_matches = people_in_self.where_row(person)
            if len(index_row_matches) == 0:  # no matches --> add person
                irow = iperson + 1  # the '+1' accounts for the header row.
                row = wcd.get_row(irow)
                self = self.append_empty_rows(1)
                for i2, i1 in hm.map2to1.items():
                    self[-1, i1] = row[i2]
                if tracker_col is not None:
                    self[-1, -1] = _tracker_True   # << tell tracker column: added this row.

        return self

    # # # REMOVE PEOPLE WHO EXITED # # #
    def remove_people_who_exited(self, *, track_n=None):
        '''return result of removing all people who exited, based on 'Enrollment status' column.
        if track_n is provided, track_n.append(number of people who were removed).
        '''
        exited_all = []
        exited_all += list(self.where_value_in_column('Enrollment status',
                                    'EXITED (will be removed next time data team runs cleanup script)'))
        exited_all += list(self.where_value_in_column('Enrollment status', 'duplicate'))
        exited_all += list(self.where_value_in_column('Job position, Spring 2024 (S24)', 'NLE'))
        exited_all += list(self.where_value_in_column('Job position, Spring 2024 (S24)', 'duplicate'))
        exited_all += list(self.where_value_in_column('Nickname', 'duplicate'))
        exited = set(exited_all)
        if track_n is not None:
            track_n.append(len(exited))
        return self.remove_rows(exited)

    def remove_people(self, people, *, missing_ok=True):
        '''remove these people from self. if not missing_ok, all these people must be in self.
        people: ThesisPerson objects
            roughly, self.remove_rows([p.row for p in people if p.caddress == self.caddress]).
        missing_ok: bool
            if True, ignore people not in self.
            if False, raise ValueError if any people not in self.
        '''
        rows = [p.row for p in people if p.caddress == self.caddress]
        if not missing_ok:
            if len(rows) != len(people):
                raise ValueError('not all people in self; self missing some people. And, missing_ok=False.')
        return self.remove_rows(rows)

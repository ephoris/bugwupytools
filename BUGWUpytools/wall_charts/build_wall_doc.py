"""
File Purpose: building wall docs

given data, build all charts appearing in this wall doc, and also make the dashboard!
"""

from .build_dashboard import rebuild_dashboard
from .build_wall_chart import rebuild_wall_sheet


''' --------------------- Defaults / Setup --------------------- '''

def rebuild_wall_doc(wdoc, thesis_data, *, docname=None, verbose=False):
    '''rebuilds wall sheets and also dashboard, for this walldoc.
    docname: None or string
        use this as the docname if provide, else use wdoc.title.
    '''
    if docname is None:
        docname = wdoc.title
    sheets = tuple(s for (d, s) in thesis_data.charts if d==docname)
    doc_people = [person for person in thesis_data.people if person.doc == docname]
    for i, sheet in enumerate(sheets):
        if verbose:
            print(f'rebuilding {i+1} of {len(sheets)} (sheet={repr(sheet)}) for doc={repr(docname)}',
                  ' '*30, end='\r')
        wsheet = wdoc[sheet]
        sheet_people = [person for person in doc_people if person.sheet == sheet]
        rebuild_wall_sheet(wsheet, sheet_people)
    if verbose:
        print(f'rebuilding dashboard for doc={repr(docname)}', ' '*40, end='\r')
    rebuild_dashboard(wdoc, thesis_data, docname=docname)
    if verbose:
        print(f'finished rebuilding doc={repr(docname)}', ' '*50, end='\r')

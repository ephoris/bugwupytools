"""
File Purpose: building a dashboard.
"""
import numpy as np

from .dashboard_formula_builder import DashboardFormulaBuilder
from . import wall_chart_defaults as defaults
from ..tools import (
    PatternError,
    CellIndex, Chart,
)


class DashboardPrefix():
    '''chart of values to the left of the dashboard formulas.
    sheet_to_dept: dict
        keys sheet_name, vals department_name.
    sheets: None or list
        tells which sheets are on this dashboard.
        None --> use list(sheet_to_dept.keys())

    self.chart will store the chart corresponding to self.
    If you update parameters in self, use self.make_chart() to remake the chart.
    '''
    HEADER = ('auto-sheet helper', 'Sheet (auto)', 'Header Range', 'Data Range', 'Sheet', 'Department')
    header_i = {key: i for i, key in enumerate(HEADER)}
    header_col = {key: CellIndex(col=i).colstr for i, key in enumerate(HEADER)}
    COLMIN, COLMAX = 'A', 'ZZ'
    HEADER_RANGE = property(lambda self: f'{self.COLMIN}1:{self.COLMAX}1', doc='''{self.COLMIN}1:{self.COLMAX}1''')
    DATA_RANGE = property(lambda self: f'{self.COLMIN}2:{self.COLMAX}', doc=''''{self.COLMIN}2:{self.COLMAX}''')

    def __init__(self, sheet_to_dept, sheets=None):
        self.sheet_to_dept = sheet_to_dept
        self._sheets = sheets
        self.make_chart()

    @property
    def sheets(self):
        '''list of sheets on this dashboard. None --> use list(self.sheet_to_dept.keys()).'''
        _sheets = self._sheets
        return list(self.sheet_to_dept.keys()) if _sheets is None else _sheets
    sheets.setter(lambda self, value: setattr(self, '_sheets', value))

    def make_chart(self):
        '''makes chart; sets self.chart then returns self.chart.'''
        self._init_chart()
        self._update_chart_sheets()
        self._update_chart_header_and_data_range_cols()

    def _init_chart(self):
        '''chart, with header.'''
        sheets, header = self.sheets, self.HEADER
        self.chart = Chart.empty((len(sheets)+1, len(header)))
        self.chart[0] = header

    def _update_chart_sheets(self):
        '''updates stuff related to sheets, in chart.'''
        chart, sheets, sheet_to_dept = self.chart, self.sheets, self.sheet_to_dept
        header_i = self.header_i
        for irow in range(1, chart.nrow):  # row 0 is header; we already set header during _init_chart.
            sheet = sheets[irow-1]  # -1 accounts for header
            dept = sheet_to_dept[sheet]
            # Auto Sheet name stuff. (auto update sheetname if it is renamed. Clever use of Formulatext()!)
            chart[irow, header_i['auto-sheet helper']] = f'={sheet}!A1'
            ash_cell = CellIndex(col=header_i['auto-sheet helper'], row=irow).indexstr
            ftxt = f"Formulatext({ash_cell})"
            chart[irow, header_i['Sheet (auto)']] = f'=Mid({ftxt}, 1+Len("="), Len({ftxt})-Len("=!A1"))'
            # "Sheet" column - formula reads from 'Sheet (auto)'. Allows to update 'Sheet' without smashing 'Sheet (auto)'
            chart[irow, header_i['Sheet']] = '=' + CellIndex(col=header_i['Sheet (auto)'], row=irow).indexstr
            # "Department" column - just data, not used internally for any calcs.
            chart[irow, header_i['Department']] = dept

    def _update_chart_header_and_data_range_cols(self):
        '''updates columns "Header Range" and "Data Range".'''
        chart, header_i = self.chart, self.header_i
        HEADER_RANGE, DATA_RANGE = self.HEADER_RANGE, self.DATA_RANGE
        sheet_col = self.header_col['Sheet']
        for irow in range(1, chart.nrow):
            chart[irow, header_i['Header Range']] = f'=${sheet_col}{irow+1}&"!{HEADER_RANGE}"'
            chart[irow, header_i['Data Range']]   = f'=${sheet_col}{irow+1}&"!{DATA_RANGE}"'

    def header_range_formula(self, irow):
        '''returns formula for accessing header range for irow (0-indexed)
        returns 'Indirect(<cell address in irow, and Header Range col. E.g. C2>)'
        '''
        cellstr = CellIndex(row=irow, col=self.header_i['Header Range']).indexstr
        return f'Indirect({cellstr})'

    def data_range_formula(self, irow):
        '''returns formula for accessing data range for irow (0-indexed)
        returns 'Indirect(<cell address in irow, and Data Range col. E.g. D2>)'
        '''
        cellstr = CellIndex(row=irow, col=self.header_i['Data Range']).indexstr
        return f'Indirect({cellstr})'

    def __repr__(self):
        return f'{type(self).__name__} with {len(self.sheets)} sheets: {self.sheets}'


class DashboardBuilder():
    '''helpful methods for building a dashboard.'''
    def __init__(self, sheet_to_dept, sheets=None):
        self.prefix = DashboardPrefix(sheet_to_dept, sheets=sheets)
        self.content = Chart.empty((self.prefix.chart.nrow, 0))
        self.dfb = DashboardFormulaBuilder(data_range="DATA_RANGE", header_range="HEADER_RANGE", row_number="ROW_NUMBER")
        self.formulas = []

    sheets = property(lambda self: self.prefix.sheets, doc='''alias to self.prefix.sheets''')
    sheet_to_dept = property(lambda self: self.prefix.sheet_to_dept, doc='''alias to self.prefix.sheet_to_dept''')

    ncol = property(lambda self: self.prefix.chart.ncol + self.content.ncol, doc='''ncol in prefix + chart''')

    @classmethod
    def from_thesis_data(cls, td, docname):
        '''returns DashboardBuilder (or subclass) for dashboard to put at docname.
        td: ThesisData object
        docname: name of google doc with data in td.
        '''
        ii = np.where(td.meta.col('Doc name', keep_header=False) == docname)[0] # if bad docname, might crash.
        meta_with_docname = td.meta[[0,*[i+1 for i in ii]]]  # 0 is header row. +1 accounts for header row.
        sheet_to_dept = meta_with_docname.lookup_dict('Sheet name', 'Department')
        sheets = tuple(s for (d, s) in td.charts if d==docname)
        return cls(sheet_to_dept, sheets=sheets)

    def formula_in_row(self, formula, irow):
        '''formula to use in irow (0-indexed row number).
        special words will be replaced. Those special words are:
            "HEADER_RANGE" --> self.prefix.header_range_formula(irow)
            "DATA_RANGE" --> self.prefix.data_range_formula(irow)
            "ROW_NUMBER" --> irow + 1
        Also, if formula doesn't yet start with '=', prepend '='.
        '''
        header_range = self.prefix.header_range_formula(irow)
        data_range = self.prefix.data_range_formula(irow)
        formula = formula.replace("HEADER_RANGE", header_range)
        formula = formula.replace("DATA_RANGE", data_range)
        formula = formula.replace("ROW_NUMBER", f'{irow+1}')
        if not formula.startswith('='):
            formula = '=' + formula
        return formula

    def append_col(self, label, formula):
        '''adds column to self.content.
        formula: string
            special words will be replaced, when using the formula in each row. Those special words are:
                "HEADER_RANGE" --> self.prefix.header_range_formula(irow)
                "DATA_RANGE" --> self.prefix.data_range_formula(irow)
                "ROW_NUMBER" --> irow + 1
            where irow is 0-indexed.
            '''
        col = [label, *(self.formula_in_row(formula, i+1) for i in range(len(self.sheets)))]  # +1 accounts for header.
        self.content = self.content.append_col_list([col])
        self.formulas.append(formula)

    def append_countif(self, label, countif_col_label, condition):
        '''appends a col which does countif(countif_col_label, condition).
        E.g. append_countif("ones", '"Assessment"', '1')
            counts number of cells equal to 1 in column "Assessment" of the sheet for this row of dashboard.
        '''
        formula = self.dfb.countif(countif_col_label, condition)
        self.append_col(label, formula)

    def append_sumleft(self, label, n_left):
        '''appends a col which does Sum of the n_left cols immediately to the left of this col.'''
        formula = f'Sum({self.dfb.get_n_left_cols(n_left)})'
        self.append_col(label, formula)

    def append_sumright(self, label, n_right):
        '''appends a col which does Sum of the n_right cols immediately to the right of this col.'''
        formula = f'Sum({self.dfb.get_n_right_cols(n_right)})'
        self.append_col(label, formula)

    def append_mathprev_explicit(self, label, formula, prevcols):
        '''appends a col which does the math in formula, for previous cols.
        all occurences of "[prevcol]" in formula are replaced with looking up that column, for prevcol in prevcols.
        '''
        formula = self.dfb.mathprev_explicit(formula, prevcols)
        self.append_col(label, formula)

    def append_mathprev(self, label, formula):
        '''appends a col which does the math in formula, replacing substrings between [] with looking up that column.
        For example, formula='[col1] - [col2]' becomes a formula which does col1[row] - col2[row] for this cell's row.
        '''
        formula = self.dfb.mathprev(formula)
        self.append_col(label, formula)

    def build(self, as_dashboard=True):
        '''returns Dashboard (or Chart) with self.prefix and self.content.
        Also sets self.chart to the Chart with self.prefix and self.content.
        as_dashboard: bool, default True
            whether to return as Dashboard, or as Chart (if False).
            if as_dashboard, equivalent to Dashboard.from_builder(self)
        '''
        if as_dashboard:
            return Dashboard.from_builder(self)
        else:
            self.chart = self.prefix.chart.append_cols(self.content)
            return self.chart


class Dashboard(Chart):
    '''chart with helpful methods for finalizing the live dashboard. (E.g., hide prefix columns.)
    [TODO] maybe some of this code should be put into Chart.
    '''
    @classmethod
    def from_builder(cls, builder):
        '''returns cls(builder.build()); also sets result.builder = builder.'''
        result = cls(builder.build(as_dashboard=False))
        result.builder = builder
        return result

    def create_dashboard_sheet(self, sheets_doc, sheet_name=defaults.DASHBOARD_NAME, *, exist_ok=True):
        '''creates sheet named 'dashboard' (i.e., sheet_name) on sheets_doc (a SheetsDoc object).
        exist_ok: bool, default True
            whether it is okay for sheet to already exist on sheets_doc.
            False --> if it did already exist, raise PatternError.
        returns sheets_doc[sheet_name].
        '''
        if sheet_name in sheets_doc.sheets:
            if exist_ok:
                return sheets_doc[sheet_name]
            else:
                errmsg = f'sheet already exists, and exist_ok=False. sheet={repr(sheet_name)}; doc={repr(sheets_doc)}'
                raise PatternError(errmsg)
        else:
            result = sheets_doc.add_worksheet(sheet_name)
            return result

    def write_to_sheet(self, wsheet, *, clear_formats=True):
        '''puts data from self into wsheet. wsheet.write_values_chart(self).
        if clear_formats, first do wsheet.clear_formats()
        '''
        wsheet._set_cached_data(self)
        with wsheet.batching(passthrough=True):
            if clear_formats:
                wsheet.clear_formats()
            result = wsheet.write_values_chart(self)
        return result

    def write_to_doc(self, sheets_doc, sheet_name=defaults.DASHBOARD_NAME, *,
                     buffer=defaults.DASHBOARD_BUFFER, format_sheet=True,
                     additional_formatting = []):
        '''put self onto sheets_doc (a SheetsDoc object), creating the sheets_chart for dashboard if necessary.
        buffer: None or (nrow, ncol) tuple
            number of empty rows & columns to put on the dashboard sheet.
            if None, don't adjust number of rows & cols.
            Otherwise, sheets_chart.set_buffer(*buffer)
        format_sheet: bool or dict, default True
            if True, self.format_sheet(result)
            if dict, self.format_sheet(result, **format_sheet)
        additional_formatting: list of callables
            if provided, call each of these, passing dashboard sheet as the only arg.
            E.g. additional_formatting[0](dsheet)
        returns sheets_doc[sheet_name].
        '''
        dsheet = self.create_dashboard_sheet(sheets_doc, sheet_name=sheet_name, exist_ok=True)
        self.write_to_sheet(dsheet)
        with dsheet.batching(passthrough=True):
            if buffer is not None:
                dsheet.set_buffer(*buffer)
            if format_sheet == True:
                self.format_sheet(dsheet)
            elif isinstance(format_sheet, dict):
                dsheet.clear_formats()
                self.format_sheet(dsheet, **format_sheet)
            for addfmt in additional_formatting:
                addfmt(dsheet)
            return dsheet

    def format_sheet(self, sheets_chart, *, clear_formats=True,
                     hide_cols=True, freeze_prefix=True,
                     freeze_header=True, header_bold=True, header_wrap=True):
        '''format dashboard sheet. kwargs allow to enable / disable certain actions.'''
        with sheets_chart.batching(passthrough=True):
            if clear_formats:
                sheets_chart.clear_formats()
            if hide_cols:
                self.hide_cols(sheets_chart)
            if freeze_prefix:
                self.freeze_prefix(sheets_chart)
            if freeze_header:
                self.freeze_header(sheets_chart)
            if header_bold and header_wrap:
                sheets_chart.row_wrap_and_bold(1)
            else:
                if header_bold:
                    sheets_chart.row_bold(1)
                if header_wrap:
                    sheets_chart.row_wrap(1)

    def hide_cols(self, sheets_chart):
        '''hides the prefix columns (before Department); show all other cols.
        (assumes sheets_chart contains dashboard data.)
        '''
        col_dept = self.builder.prefix.chart.ncol
        with sheets_chart.batching(passthrough=True):
            sheets_chart.hide_dimensions(start=1, end=col_dept-1, dimension='COLUMNS')
            sheets_chart.show_dimensions(col_dept, end=self.builder.chart.ncol, dimension='COLUMNS')

    def freeze_prefix(self, sheets_chart):
        '''freezes columns up to end of prefix.'''
        sheets_chart.frozen_cols = self.builder.prefix.chart.ncol

    def freeze_header(self, sheets_chart):
        '''freezes row 1.'''
        sheets_chart.frozen_rows = 1

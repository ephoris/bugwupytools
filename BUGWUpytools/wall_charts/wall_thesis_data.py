"""
File Purpose: WallThesisData
"""
import os
import collections
import functools

import numpy as np

from .people import (
    ThesisPersonInfo, ThesisPerson,
    PersonList,
)
from .wall_chart_data import (
    _from_excel_paramdocs_nofile,
)
from .wall_doc_data import (
    WallDocData,
)
from .wall_metadata import (
    WallMetadata,
)
from ..tools import (
    InputConflictError,
    InDirectory, choose_file,
    Chart,  # <-- for type checking
    display_with_clickable_urls,
    similarity,
    format_docstring, alias, alias_to_result_of,
)
from . import wall_chart_defaults as defaults


class WallThesisData():
    '''for managing one "state" of all the wall docs and all their wall charts.
    all wall chart data as well as a meta-info file for urls / sheet names / doc titles.

    an instance contains all info from all the wall charts at a given point in time.

    wdds: dict of {doc name: WallDocData object from doc}
    meta: WallMetadata
    '''
    def __init__(self, wdds, meta):
        self._wdds = wdds
        self.meta = meta
        self._state = 0

    @property
    def wdds(self):
        '''dict of {docname: WallDocData object from doc}.'''
        return self._wdds
    @wdds.setter
    def wdds(self, value):
        '''increments self._state each time wdds is changed.'''
        self._wdds = value
        self._state += 1

    def __setitem__(self, doc_and_sheet, value):
        '''sets self.wdds[doc][sheet]=value, then increments self._state.'''
        doc, sheet = doc_and_sheet
        wdds = self.wdds
        wdds[doc][sheet] = value
        self.wdds = wdds   # << this line also increments self._state; see the @wdds.setter method above.

    def _cache_with_state(f):
        '''return function which does f(self, *args, **kw) but also caching, considering self._state.'''
        cache_attr = f'_cached_{f.__name__}'
        @functools.wraps(f)
        def f_but_cache_with_state(self, *args, **kw):
            try:
                result = getattr(self, cache_attr)  # (state, charts_dict)
            except AttributeError:
                pass  # handled after the else block.
            else:
                if result[0] == self._state:
                    return result[1]
            result = f(self, *args, **kw)
            setattr(self, cache_attr, (self._state, result))
            return result
        return f_but_cache_with_state

    @classmethod
    def from_wallthesis(cls, wallthesis, _debug=False, **kw__from_walldoc):
        '''initialize self from a WallThesis object.
        Note: this involves reading the data from every WallDoc object.
        Might take some time.
        if _debug, only read the first 5 wall docs, to test the code without waiting for google.
        '''
        docs_dict = wallthesis.docs_dict()
        wdds = dict()
        L = len(docs_dict)
        fmtstr = f'{{:{max(len(key) for key in docs_dict.keys())}s}}'
        if L > 10 and not _debug:
            print("reading docs, may sometimes stall for ~1 minute if google mad at us for too many requests.")
        for i, (key, wd) in enumerate(docs_dict.items()):
            if _debug and i>=5:
                break
            print(f'reading doc {i+1:2d} of {L}:', fmtstr.format(key), end='\r')
            wdds[key] = WallDocData.from_walldoc(wd, **kw__from_walldoc)
        return cls(wdds, meta=wallthesis.meta)

    # # # DICT-LIKE, but use tuples: # # #
    def keys(self):    return tuple(self.wdds.keys())
    def values(self):  return tuple(self.wdds.values())
    def items(self):   return tuple(self.wdds.items())
    def __len__(self): return len(self.wdds)

    def chart_items(self):  return tuple(self.charts.items())

    # # # INDEXING # # #
    def __getitem__(self, key):
        '''dict-like indexing if key is a string; else list-like indexing of self.list_names().
        key: string or int or tuple or slice
            string --> return WallDocData for doc with name key. I.e. self.wdds[key].
            tuple --> return self.charts[key]
                in this case, key should be (docname, sheetname)
            int or slice --> return self.person_from_index(key).
                Indexes self.list_people(), a list of ThesisPerson objects.
        '''
        if isinstance(key, str):
            return self.wdds[key]
        elif isinstance(key, tuple):
            return self.charts[key]
        else:
            return self.person_from_index(key)

    ndocs = property(lambda self: len(self.wdds), doc='''number of WallDocData objects in self.''')
    ncharts = property(lambda self: len(self.list_charts()), doc='''number of WallChartData objects in self.''')
    npeople = property(lambda self: len(self.list_people_info()),
                       doc='''total number of people across all charts from self.''')
    nrows = alias('npeople')

    # # # STRINGS # # #
    def __repr__(self):
        return f'{type(self).__name__}({len(self)} WallDocs with titles: {self.keys()})'

    # # # EXCEL READ / WRITE # # #
    def to_excel(self, name=None, *, dir=None, **kw__write):
        '''saves wall thesis (i.e. a backup of all the wall docs) to local machine.
        returns abspath of the folder created to store the thesis.

        name: if None, use wall_chart_defaults.THESIS_NAME   (probably 'wallcharts_backup')
        '''
        if len(self) == 0:
            raise IndexError('cannot write empty WallThesisData object.')
        # need info from the first item to determine thesis_dst (we put t_read into it.)
        iter_items = iter(self.items())
        key, wdd = next(iter_items)
        dir = defaults.DEFAULT_SAVEDIR if dir is None else dir
        if name is None: name = defaults.THESIS_NAME
        thesis_name = f'{name}{wdd._get_t_read_string()}'
        thesis_dst = os.path.join(dir, thesis_name)
        os.makedirs(thesis_dst, exist_ok=True)
        # write the metadata object to excel
        self.meta.to_excel(dir=thesis_dst, **kw__write)
        # write the WallDocData objects to excel
        _absdst = wdd.to_excel(dir=thesis_dst, **kw__write)
        L = len(self)
        for i, (key, wdd) in enumerate(iter_items):
            print(f'writing to excel {i+2:2d} of {L}', end='\r')
            wdd.to_excel(dir=thesis_dst, **kw__write)
        return os.path.abspath(thesis_dst)

    @classmethod
    @format_docstring(paramdocs=_from_excel_paramdocs_nofile.replace('file', 'folder'))
    def from_excel(cls, foldername=None, metaname=None, dir=None, choice_method='user',
                   _missing_ok=False, **kw__read):
        '''read all excel files from folder foldername, returning a WallThesisData object.

        foldername: None or str
            start of foldername, or full foldername.
            None --> use defaults.THESIS_NAME
            start of foldername --> choose a folder starting with this string.
                (may prompt user if there are multiple options; see choice_method)
        metaname: None or str
            (start of, or full) filename of meta file, within folder foldername.
            None --> use defaults.DEFAULT_META_NAME
            start of filename --> choose a file starting with this string.
                (may prompt user if there are multiple options; see choice_method)
        {paramdocs}
        _missing_ok: bool, default False
            whether it is okay for files in meta to be missing from the folder.
        '''
        if dir is None:
            dir = defaults.DEFAULT_SAVEDIR
        with InDirectory(dir):
            if foldername is None: foldername = defaults.THESIS_NAME
            foldername = choose_file(startswith=foldername, choice_method=choice_method)
            with InDirectory(foldername):
                kw_ = dict(dir='.', choice_method=choice_method, **kw__read)
                # read meta
                if metaname is None: metaname = defaults.DEFAULT_META_NAME
                meta = WallMetadata.from_excel(metaname, **kw_)
                docnames = meta.unique_doc_names()
                # read docs indicated by meta
                wdds = dict()
                L = len(docnames)
                for i, docname in enumerate(docnames):
                    print(f'reading excel doc {i+1:2d} of {L}', end='\r')
                    try:
                        wdd = WallDocData.from_excel(docname, **kw_)
                    except FileNotFoundError:
                        if not _missing_ok:
                            raise
                    else:
                        wdds[docname] = wdd
        return cls(wdds, meta=meta)

    # # # INSPECT CHARTS # # #
    __iter__ = alias('iter_charts')

    def get_chart(self, docname, sheetname):
        '''returns self[docname][sheetname], which is a WallChart object.'''
        return self[docname][sheetname]

    def iter_charts(self, meta_only=True, missing_ok=False, include_caddress=False, **kw__meta_iter_docs):
        '''iterates through self, yielding each WallChart object.
        meta_only: bool, default True
            True --> only use the charts indicated by self.meta.
            False --> use all sheets from each WallDoc in self.
        missing_ok: bool, default False
            if meta_only, missing_ok tells whether it is ok for a chart to be missing.
            if False, crash with KeyError if a doc is missing. Else skip that chart.
        include_caddress: bool, default False
            if True, yield (caddress, wallchart) instead of just wallchart.
            caddress = (doc_name, sheet_name) from wallchart.
        '''
        if meta_only:
            for docinfo in self.meta.iter_docs(**kw__meta_iter_docs):
                docname = docinfo.docname
                sheets = docinfo.sheets
                for sheetname in sheets:
                    try:
                        wallchart = self.get_chart(docname, sheetname)
                    except KeyError:
                        if not missing_ok:
                            raise
                    yield ((docname, sheetname), wallchart) if include_caddress else wallchart
        else:
            for docname, walldoc in self.items():
                for sheetname, wallchart in walldoc.items():
                    yield ((docname, sheetname), wallchart) if include_caddress else wallchart

    @_cache_with_state
    def list_charts(self):
        '''returns list of all WallChartData objects in self.
        Equivalent to self.iter_charts(include_caddress=False).
        '''
        return [chart for chart in self.iter_charts(include_caddress=False)]

    @_cache_with_state
    def charts_dict(self):
        '''returns dict of {(docname, sheetname): WallChart for chart in self}.
        only includes the charts indicated by self.meta.
        uses caching to reduce time required to produce this dict.
        '''
        return {caddress: chart for caddress, chart in self.iter_charts(meta_only=True, include_caddress=True)}

    charts = alias_to_result_of('charts_dict')

    def which_charts(self, f, **kw__iter_charts):
        '''returns (docname, sheetname) for chart in self satisfying f(chart).'''
        return tuple(address for address, chart in
                     self.iter_charts(**kw__iter_charts, include_caddress=True) if f(chart))

    def charts_with(self, f, **kw__iter_charts):
        '''returns chart for chart in self satisfying f(chart).'''
        return tuple(chart for chart in
                     self.iter_charts(**kw__iter_charts, include_caddress=False) if f(chart))

    def check_charts(self, f, **kw__iter_charts):
        '''returns dict of {(docname, sheetname): f(chart) for chart in self}.'''
        return {address: f(chart) for address, chart in
                self.iter_charts(**kw__iter_charts, include_caddress=True)}

    def get_url(self, chart_or_docname_or_tuple):
        '''returns url corresponding to this object.
        Chart --> docname = obj.doc_name
        tuple --> docname = obj[0]
        docname --> self.meta.get_url(docname)
        '''
        x = chart_or_docname_or_tuple
        if isinstance(x, Chart):
            docname = x.doc_name
        elif isinstance(x, tuple):
            docname = x[0]
        else:
            docname = x
        return self.meta.get_url(docname)

    # # # EDIT CHARTS # # #
    def charts_apply(self, f, **kw__iter_charts):
        '''updates all charts in self via f.
        The charts stored in self will point to the result of f,
            e.g. self[doc][sheet] = f(chart at (doc, sheet)).
        if f crashes for any chart, abort without making any changed to self.
        '''
        list_ = list(self.iter_charts(**kw__iter_charts, include_caddress=True))  # << get all before adjusting any.
        # do all calculations first (to ensure none crash) before making changes to self.
        result = dict()
        for caddress, chart in list_:
            result[caddress] = f(chart)
        # update self with the results.
        for caddress, chart in result.items():
            self[caddress] = chart

    def append_col_if_missing(self, label, **kw__iter_charts):
        '''updates all charts in self to have a column with the name provided.
        If a column with that name already exists, don't edit the chart.
        Otherwise, append a column with row 0 value equal to colname.
        '''
        def _appending_col_if_missing(chart):
            return chart.append_col_if_missing(label)
        return self.charts_apply(_appending_col_if_missing, **kw__iter_charts)

    def relabel_col(self, old_label, new_label, *, only_first=True, missing_ok=True, copy=True, **kw__iter_charts):
        '''updates all charts in self by replacing the first occurence of old_label in row 0 with new_label.

        only_first: bool, default True
            whether to replace only the first occurrence, if multiple are found.
            False --> replace all occurrences.
        missing_ok: bool, default True
            whether it's okay if old_label is missing in row 0.
            False --> make KeyError if old_label is missing.
        copy: bool, default True
            whether to copy each chart before relabeling.
            True --> operation is performed on copy
            False --> operation is performed in-place, i.e. chart is changed directly.
        '''
        def _relabeling_col(chart):
            return chart.relabel_col(old_label, new_label,
                                     only_first=only_first, missing_ok=missing_ok, copy=copy)
        return self.charts_apply(_relabeling_col, **kw__iter_charts)

    def set_column_val(self, label, people, missing_ok=False):
        '''sets value in column with this label, for each person in people_dict.
        each person in people must have:
            person.raddress tells (doc name, sheet name, row index)
            where row index is 0 for the first row of data (as if there were no header).
            person[label] tells the value to write to the cell in this column at (doc, sheet, row).
        missing_ok: bool, default False
            whether it is ok for some of the persons in people to not have person[label].
        '''
        for person in people:
            doc, sheet, row = person.raddress
            try:
                value = person[label]
            except KeyError:
                if missing_ok:
                    continue  # skip this person
                else:
                    raise
            chart = self[doc][sheet]
            icol = chart.column_index_from_label(label)
            chart[row+1, icol] = value   # row+1 to account for header.
        self._state += 1   # charts have been edited; ensure caching doesn't return old values.

    # # # INSPECT PEOPLE # # #
    def _iter_people_info(self, **kw__iter_charts):
        '''yielding ThesisPersonInfo(self, docname, sheetname, row_index, first name, last name) from self.

        first name from 'First name' column; last name from 'Last name' column.
        Skips all no-name rows, i.e. rows with blank first name and blank last name.

        future-proofing:
            promise: ThesisPersonInfo(...)[:3] will always be docname, sheetname, row_index
            possible change: remaining info in ThesisPersonInfo may change,
                e.g. might remove name or add VAN id.
        '''
        for (doc, sheet), chart in self.iter_charts(**kw__iter_charts, include_caddress=True):
            for irow, (first, last) in enumerate(chart.columns('First name', 'Last name')):
                if first != '' or last != '':
                    yield ThesisPersonInfo(self, doc, sheet, irow, first, last)

    @_cache_with_state
    def list_people_info(self):
        '''returns np.array(self._iter_people_info(meta_only=True, missing_ok=False), dtype=object).
        Each element is a ThesisPersonInfo object.
        uses caching to reduce time required to produce this list.
        '''
        list_ = list(self._iter_people_info(meta_only=True, missing_ok=False))
        return np.array(list_, dtype=object)

    @_cache_with_state
    def list_people(self):
        '''returns PersonList(person_info.get_person() for person_info in self.list_people_info())
        Each element is a ThesisPerson object.
        uses caching to reduce time required to produce this list.
        [EFF] note: no need for further efficiency; takes <0.5 seconds for ~5000 people.
        '''
        list_ = [pi.get_person() for pi in self.list_people_info()]
        return PersonList(list_)

    people = alias_to_result_of('list_people')

    def person_info_from_index(self, i):
        '''returns self.list_people_info[i]. That list is a list of ThesisPersonInfo objects.'''
        return self.list_people_info()[i]

    def person_from_index(self, i):
        '''returns self.list_people[i]. That list is a list of ThesisPerson objects.'''
        return self.list_people()[i]

    def get_row(self, docname, sheetname, row_index, *args__None, labeled=True):
        '''returns Chart from self at docname and sheetname, showing only row 0 and row_index+1
        (row_index is the 0-indexed n'th row of data; the +1 accounts for the "header" row.)

        labeled: bool, default True
            if False, instead return only row_index+1, as an array rather than a chart.

        See also: ThesisPersonInfo.get_row()
        '''
        chart = self[docname][sheetname]
        if labeled:
            return chart[[0, row_index+1], :]
        else:
            return chart.get_row(row_index+1)

    def getvals(self, *keys, asarray=True, dtype=None, **kw__person_getvals):
        '''returns [person.getvals(*keys, **kw) for person in self.people].
        asarray: bool, default True
            whether to convert result to numpy array.
        dtype: passed directly to np.array(...) as kwarg, if asarray.
        '''
        result = [person.getvals(*keys, **kw__person_getvals) for person in self.people]
        return np.array(result, dtype=dtype) if asarray else result

    def getval(self, key, asarray=True, dtype=None, **kw__person_getvals):
        '''returns [person.getvals(key, **kw)[0] for person in self.people]
        asarray: bool, default True
            whether to convert result to numpy array.
        dtype: passed directly to np.array(...) as kwarg, if asarray.
        '''
        result = [person.getvals(key, **kw__person_getvals)[0] for person in self.people]
        return np.array(result, dtype=dtype) if asarray else result

    # # # INSPECT HEADERS # # #
    def common_header(self, **kw__iter_charts):
        '''returns list of all keys which appear in the header of ALL charts in self.'''
        iter_charts = self.iter_charts(include_caddress=False, **kw__iter_charts)
        chart0 = next(iter_charts)  # will crash if self has no charts.. that's fine though.
        result = chart0.header
        for chart in iter_charts:
            header = chart.header
            result = [key for key in result if key in header]
        return result

    def uncommon_header(self, **kw__iter_charts):
        '''returns list of keys which appear in the header of at least 1, but not all, charts in self.'''
        hka = self.count_header_key_appearances(**kw__iter_charts)
        result = [key for key, val in hka.items() if val < self.ncharts]
        return result

    def count_header_key_appearances(self, duplicates=False, sort=True, **kw__iter_charts):
        '''returns dict of {key: number of times key appears in any header} for keys in any header.
        duplicates: bool, default False
            whether to count a key multiple times if it appears in the SAME header.
            False --> only count each key at most one time per header.
        '''
        result = collections.defaultdict(int)
        for chart in self.iter_charts(include_caddress=False, **kw__iter_charts):
            header = chart.header if duplicates else np.unique(chart.header)
            for key in header:
                result[key] += 1
        result = dict(result)
        if sort:
            result = {key: result[key] for key in sorted(result.keys())}
        return dict(result)

    hka = alias('count_header_key_appearances')

    def count_header_key_appearances_and_data(self, sort=True, **kw__count_hka):
        '''returns dict of {key: (number of times key appears in any header,
                                  number non-blank values in col labeled with key,
                                  number blanks in col labeled with key)
        The number of rows total with key is sum of number of rows on charts with key in header.
        '''
        hka = self.count_header_key_appearances(sort=sort, **kw__count_hka)
        result = dict()
        for key, happ in hka.items():
            vals = self.getval(key, default=None)
            nrows_thesis   = len(vals)
            nrows_None     = np.count_nonzero(vals==None)  # rows not having key in header.
            nrows_blank    = np.count_nonzero(vals=='')
            nrows_nonblank = nrows_thesis - nrows_None - nrows_blank
            result[key] = (happ, nrows_nonblank, nrows_blank)
        return result

    hkad = alias('count_header_key_appearances_and_data')

    def rare_header_caddresses(self, threshold=5, **kw__iter_charts):
        '''returns dict of {key: [(docname, sheetname) with key in header] for rare keys}.
        threshold: int, default 5
            key is "rare" if it appears in at most this many headers.
        '''
        hka = self.count_header_key_appearances(**kw__iter_charts)
        rarekeys = [key for key, val in hka.items() if val <= threshold]
        result = collections.defaultdict(list)
        for caddress, chart in self.iter_charts(include_caddress=True, **kw__iter_charts):
            header = chart.header
            for key in rarekeys:
                if key in header:
                    result[key].append(caddress)
        return dict(result)

    def missing_header_caddresses(self, threshold=5, **kw__iter_charts):
        '''returns dict of {key: [(docname, sheetname) with key not in header] for "missing" keys}
        threshold: int, default 5
            key is "missing" if it is missing from at most this many headers, but appears in all others.
        '''
        hka = self.count_header_key_appearances(**kw__iter_charts)
        missingkeys = [key for key, val in hka.items() if (0 < (self.ncharts - val) <= threshold)]
        result = collections.defaultdict(list)
        for caddress, chart in self.iter_charts(include_caddress=True, **kw__iter_charts):
            header = chart.header
            for key in missingkeys:
                if key not in header:
                    result[key].append(caddress)
        return dict(result)

    def rare_header_urls(self, threshold=5, duplicates=False, sim_threshold=0.7, **kw__iter_charts):
        '''displays string with clickable urls to help deal with rare headers.'''
        lines = []
        rhc = self.rare_header_caddresses(threshold=threshold, **kw__iter_charts)
        hka = self.count_header_key_appearances(duplicates=duplicates, **kw__iter_charts)
        for key, cads in sorted(rhc.items()):
            sim = self.similar_header_keys(key, threshold=sim_threshold)
            simstr = ', '.join(f'{repr(k)} ({hka[k]})' for k in sim)  # e.g. 'key' (80) if key appears on 80 charts.
            lines += [f'{repr(key)}: (guesses: [{simstr}])']
            for cad in cads:
                lines += [f'  {cad}', f'    {self.get_url(cad)}']
        result = '\n'.join(lines)
        display_with_clickable_urls(result)

    rare_headers = alias('rare_header_urls')

    def missing_header_urls(self, **kw):
        '''displays string with clickable urls to help deal with missing headers.'''
        lines = []
        mhc = self.missing_header_caddresses(**kw)
        for key, cads in sorted(mhc.items()):
            lines += [f'{repr(key)}:']
            for cad in cads:
                lines += [f'  {cad}', f'    {self.get_url(cad)}']
        result = '\n'.join(lines)
        display_with_clickable_urls(result)

    missing_headers = alias('missing_header_urls')

    def similar_header_keys(self, key, threshold=0.7, eq=False):
        '''returns all keys from self.uncommon_header() which are similar to key.
        eq: bool, default False
            whether to include key in the result (or anything EQUAL to key).
            False --> result will only have things which are similar and NOT equal.
        '''
        uh = self.uncommon_header()
        return [k for k in uh if ((eq or (k!=key)) and (similarity(k, key) > threshold))]

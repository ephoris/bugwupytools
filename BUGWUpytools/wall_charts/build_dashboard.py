"""
File Purpose: building dashboard (for one wall doc)

This file should be updated manually to edit which columns go into the dashboard.

For functions here, bb refers to a DashboardBuilder (i.e. just for building the chart)
while dashboard refers to a Dashboard (has helper methods for writing to google doc)
"""
from .build_wall_chart import _apply_border_right, AA
from .dashboard_builder import DashboardBuilder


''' --------------------- Defaults / Setup --------------------- '''

# # COLUMN WIDTHS # #
# To get this, set up one test dashboard and adjust the widths manually.
# use empty dict to use standard width for all cols. i.e. COL_WIDTHS = dict().
# Then, use WallChart.get_header_widths().
COL_WIDTHS = {'auto-sheet helper': 75,
              'Sheet (auto)': 88,
              'Header Range': 127,
              'Data Range': 123,
              'Sheet': 97,
              'Department': 99,
              'Workers': 63,
              'with assigned rep(s)': 97,
              'Assessed (union support)': 75,
              '1 - Leader': 52,
              '2 - Supporter': 71,
              '3 - Unsure': 55,
              '4 - Hostile': 52,
              '5 - Anti-union Leader': 94,
              'Instructor of Record': 70,
              'TA or TF': 50,
              'grader': 50,
              'RA or RF': 50,
              'Dean\'s Fellow': 50,
              'Dissertation Fellow': 70,
              'other': 50,
              'On Leave': 50,
              'Assessed (commitment to vote in SAV)': 100,
              '(SAV) 1 - Committed to vote yes': 118,
              '(SAV) 2 - Leaning towards voting yes': 118,
              '(SAV) 3 - Unsure': 118,
              '(SAV) 4 - Planning to not vote or vote no': 118,
              '(SAV) 0 - Cannot vote': 118,
              'Assessed (willingness to strike research)': 106,
              'Assessed (willingness to refuse to teach)': 106,
              '(refuse to teach) 1 - Enthusiastic': 111,
              '(refuse to teach) 2 - Hesitant but willing': 111,
              '(refuse to teach) 3 - Unsure': 111,
              '(refuse to teach) 4 - Not willing': 111,
              '(refuse to teach) 0 - N/A': 111,
              'Assessed (willingness to withhold grades)': 114,
              '(withhold grades) 1 - Enthusiastic': 118,
              '(withhold grades) 2 - Hesitant but willing': 118,
              '(withhold grades) 3 - Unsure': 118,
              '(withhold grades) 4 - Not willing': 118,
              '(withhold grades) 0 - N/A': 118,
              '(strike prep attendance) 0 meetings': 82,
              '(strike prep attendance) 1 meeting': 82,
              '(strike prep attendance) 2+ meetings': 84,
              'Union reps (current)': 77,
              '(2023-24) recommited': 82,
              '(2023-24) new': 64,
              '(2022-23) might recommit': 106,
              '(2022-23) not recommitting': 95,
              'PhDs': 42,
              'MAs': 36,
              'PhD/MA Unknown': 70,
              'Leaving in <1 month': 73,
              'Left <1 month ago': 77,
              'Left >1 month ago': 76,
              'Voted in SAV': 64,
              }


''' --------------------- Building Dashboard Chart --------------------- '''

DASHBOARD_COLS = []
def dashboard_cols(f):
    '''add f to list of dashboard cols, then return f.
    Use '---' to indicate 'put a border here'.
    '''
    DASHBOARD_COLS.append(f)
    return f

def build_dashboard_chart(bb):
    '''builds chart with all dashboard cols. returns bb.build()
    sets result._border_right_cols = list of columns (ints, 1-indexed) for where to put right-side border.
    '''
    border_cols = []
    for colf in DASHBOARD_COLS:
        if colf == '---':
            border_cols.append(bb.ncol)
        else:
            colf(bb)
    result = bb.build()
    result._border_right_cols = border_cols
    return result

@dashboard_cols
def worker_col(bb):
    '''counting number of workers.'''
    bb.append_countif('Workers', '"First name"', '"<>"')  # non-blank first name.

@dashboard_cols
def assigned_reps_col(bb):
    '''counting number of workers with assigned reps.'''
    bb.append_countif('with assigned rep(s)', '"Assigned Rep(s)"', '"<>"')

dashboard_cols('---')

@dashboard_cols
def assessment_cols(bb):
    '''tracking assessments.'''
    assessments = ("1 - Leader",
                   "2 - Supporter",
                   "3 - Unsure",
                   "4 - Hostile",
                   "5 - Anti-union Leader")
    bb.append_sumright('Assessed (union support)', len(assessments))  # sum the n=len(assessments) cols to the right.
    for assess in assessments:
        bb.append_countif(assess, '"Assessment"', f'"{assess}"')

dashboard_cols('---')

@dashboard_cols
def job_position_cols(bb):
    '''tracking job position for S24'''
    choices = ('Instructor of Record',
               'TA or TF',
               'grader',
               'RA or RF',
               'Dean\'s Fellow',
               'Dissertation Fellow',
               'other',
               'On Leave')
    for choice in choices:
        bb.append_countif(choice, f'"{AA["job"]}"', f'"{choice}"')

dashboard_cols('---')

@dashboard_cols
def SAV_vote_cols(bb):
    '''tracking commitment to vote in SAV.'''
    assessments = ("1 - Committed to vote yes",
                   "2 - Leaning towards voting yes",
                   "3 - Unsure",
                   "4 - Planning to not vote or vote no",
                   "0 - Cannot vote")
    bb.append_sumright('Assessed (commitment to vote in SAV)', len(assessments))  # sum the n=len(assessments) cols to the right.
    for assess in assessments:
        bb.append_countif(f"(SAV) {assess}", f'"{AA["will_vote_SAV"]}"', f'"{assess}"')

dashboard_cols('---')

@dashboard_cols
def strike_ready_teach_cols(bb):
    '''tracking strike readiness for refusing to teach.'''
    assessments = ("1 - Enthusiastic",
                   "2 - Hesitant but willing",
                   "3 - Unsure",
                   "4 - Not willing",
                   "0 - N/A")
    bb.append_sumright('Assessed (willingness to refuse to teach)', len(assessments))  # sum the n=len(assessments) cols to the right.
    for assess in assessments:
        bb.append_countif(f"(refuse to teach) {assess}", f'"{AA["will_strike_teach"]}"', f'"{assess}"')

dashboard_cols('---')

@dashboard_cols
def strike_ready_grades_cols(bb):
    '''tracking strike readiness for withholding grades.'''
    assessments = ("1 - Enthusiastic",
                   "2 - Hesitant but willing",
                   "3 - Unsure",
                   "4 - Not willing",
                   "0 - N/A")
    bb.append_sumright('Assessed (willingness to withhold grades)', len(assessments))  # sum the n=len(assessments) cols to the right.
    for assess in assessments:
        bb.append_countif(f"(withhold grades) {assess}", f'"{AA["will_strike_grade"]}"', f'"{assess}"')

dashboard_cols('---')

@dashboard_cols
def strike_prep_attendance_cols(bb):
    '''tracking '''
    choices = ('0 meetings',
               '1 meeting',
               '2+ meetings')
    for choice in choices:
        bb.append_countif(f"(strike prep attendance) {choice}", f'"{AA["#meetings"]}"', f'"{choice}"')

dashboard_cols('---')

@dashboard_cols
def union_reps_col(bb):
    '''tracking union reps.'''
    choices = (('(2023-24) recommited', "current (2023-24), recommitted (after 2022-23)"),
               ('(2023-24) new', "current (2023-24), new this year"),
               ('(2022-23) might recommit', "prior (2022-23), might recommit"),
               ('(2022-23) not recommitting', "prior (2022-23), not recommitting"))
    bb.append_sumright('Union reps (current)', 2)  # add number of 2023-24 reps.
    for (title, choice) in choices:
        bb.append_countif(title, '"Union Rep?"', f'"{choice}"')

dashboard_cols('---')

@dashboard_cols
def ma_phd_cols(bb):
    '''tracking number of MA/PhD.'''
    bb.append_countif('PhDs', '"MA/PhD"', '"PhD"')
    bb.append_countif('MAs', '"MA/PhD"', '"MA"')
    bb.append_mathprev('PhD/MA Unknown', '[Workers] - [PhDs] - [MAs]')

@dashboard_cols
def enrollment_cols(bb):
    '''tracking enrollment'''
    choices = (('Leaving in <1 month', "Expect <1 month before leaving"),
               ('Left <1 month ago', "Left <1 month ago"),
               ('Left >1 month ago', "Left >1 month ago"))
    for (title, choice) in choices:
        bb.append_countif(title, '"Enrollment status"', f'"{choice}"')

dashboard_cols('---')

@dashboard_cols
def voted_SAV(bb):
    '''tracking who voted in SAV.'''
    bb.append_countif('Voted in SAV', '"Voted in SAV?"', 'True')

dashboard_cols('---')

@dashboard_cols
def strike_ready_research_cols(bb):
    '''tracking strike readiness for refusing to research.'''
    assessments = ("? - Interested but no established plan yet",
                   "1 - Enthusiastic",
                   "2 - Hesitant but willing",
                   "3 - Unsure",
                   "4 - Not willing",
                   "0 - N/A")
    bb.append_sumright('Assessed (willingness to strike research)', len(assessments))  # sum the n=len(assessments) cols to the right.
    for assess in assessments:
        bb.append_countif(f"(strike research) {assess}", f'"{AA["will_strike_research"]}"', f'"{assess}"')

dashboard_cols('---')


''' --------------------- Building Dashboard - Writing to WallDoc --------------------- '''

def format_dsheet_standard__maker(dchart=None):
    '''returns func which does "standard" formatting for dsheet'''
    _border_right_cols = dchart._border_right_cols if dchart is not None else []
    def format_dsheet_standard(dsheet):
        for bcol in dchart._border_right_cols:
            _apply_border_right(dsheet, bcol)
        dsheet.set_header_widths(COL_WIDTHS)
    return format_dsheet_standard

def rebuild_dashboard(wdoc, thesis_data, *, docname=None):
    '''builds dashboard sheet on WallDoc wdoc.
    docname: None or string
        if provided, use this as the docname (dashboard for WallCharts in this doc.)
        otherwise, use wdoc.title as the docname.
    returns WallChart containing the dashboard sheet.
    '''
    if docname is None:
        docname = wdoc.title
    bb = DashboardBuilder.from_thesis_data(thesis_data, docname)
    dchart = build_dashboard_chart(bb)
    if dchart.nrow == 1:
        print(f'WARNING: No sheets found for docname provided. (docname={repr(docname)})')
    standard_fmt = format_dsheet_standard__maker(dchart)
    dsheet = dchart.write_to_doc(wdoc, additional_formatting=[standard_fmt])
    return dsheet

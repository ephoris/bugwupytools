"""
File Purpose: WallDocData class
"""
from .wall_chart_data import (
    WallChartData, _from_excel_paramdocs,
)

from ..tools import (
    _get_excel_filename,
    strtime,
    InDirectory, choose_file,
    format_docstring,
)
from . import wall_chart_defaults as defaults

class WallDocData():
    '''Data from possibly multiple wall charts. Not connected to a google doc.
    Use self.wcds to get a dict with the underlying WallChartData objects.
    Provides some "standard" methods for manipulating the data.

    wcds: dict of {sheet title: WallChartData object from sheet}
    '''
    def __init__(self, wcds):
        self.wcds = wcds

    @classmethod
    def from_walldoc(cls, walldoc, sheets=None, exclude_sheets=None, **kw__from_wallchart):
        '''initialize self from WallDoc object.
        sheets: None or iterable which allows repeated iteration, e.g. list
            if provided, only use the sheets with title in this list,
            and ensure that all sheets from this list are present in the result.
            i.e. set(result.keys()) == set(sheets) is guaranteed.
            raise PatternError if that is not the case.
        exclude_sheets: None or iterable which allows repeated iteration, e.g. list
            if provided, exclude any sheets with title in this list.
        '''
        sheets_dict = walldoc.sheets_dict(sheets=sheets, exclude_sheets=exclude_sheets)
        wcds = {key: WallChartData.from_wallchart(wc, **kw__from_wallchart)
                for key, wc in sheets_dict.items()}
        return cls(wcds)

    # # # DICT-LIKE, but use tuples: # # #
    def keys(self):    return tuple(self.wcds.keys())
    def values(self):  return tuple(self.wcds.values())
    def items(self):   return tuple(self.wcds.items())
    def __len__(self): return len(self.wcds)
    def __getitem__(self, key):  return self.wcds[key]
    def __setitem__(self, key, value):  self.wcds[key] = value

    # # # STRINGS # # #
    def __repr__(self):
        return f'{type(self).__name__}({len(self)} WallCharts with titles: {self.keys()})'

    def _get_t_read_minkey(self):
        '''returns key of wall chart data with earliest t_read in self.
        if all read times are None, return key of any wall chart data in self.
        if len(self) == 0, raise IndexError.
        '''
        if len(self) == 0:
            raise IndexError('cannot get t_read from empty WallChartData object.')
        times = tuple((key, wcd.t_read) for key, wcd in self.items() if wcd.t_read is not None)
        if len(times) == 0:
            return self.keys()[0]  # return any key.
        else:
            minkey, mintime = min(times, key=lambda key_and_time: key_and_time[1])
            return minkey

    def _get_t_read_string(self):
        '''returns t_read string of wall chart data object with minimum read time from self'''
        minkey = self._get_t_read_minkey()
        return self[minkey]._get_t_read_string()

    # # # EXCEL READ / WRITE # # #
    def to_excel(self, dst=None, dir=None, dryrun=False, **kw__write):
        '''saves wall doc to local machine at dst.
        if dst not provided, use dst implied by one of the wall chart data objects in self.
        
        dryrun: bool, default False
            if dryrun, don't actually write anything! But still return the same thing.

        returns abspath of dst.
        '''
        if len(self) == 0:
            raise IndexError('cannot write empty WallChartData object to excel.')
        iter_items = iter(self.items())
        key, wcd = next(iter_items)
        absdst, _sheet = wcd.to_excel(dst=dst, dir=dir, dryrun=dryrun, **kw__write)
        for key, wcd in iter_items:
            wcd.to_excel(dst=absdst, dryrun=dryrun, **kw__write)  # absdst --> same file as earlier sheets.
        return absdst

    @classmethod
    @format_docstring(paramdocs=_from_excel_paramdocs)
    def from_excel(cls, filename, dir=None, choice_method='user', **kw__read):
        '''read all sheets from excel file at filename, returning a WallDocData object.

        {paramdocs}
        '''
        wcds = WallChartData.from_excel(filename, sheet_name=None,
                                        dir=dir, choice_method=choice_method, **kw__read)
        return cls(wcds)
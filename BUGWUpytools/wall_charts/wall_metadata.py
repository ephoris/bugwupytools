"""
File Purpose: WallMetadata

google doc stores metadata about wall charts
"""
import warnings
import collections
import functools

import numpy as np

from .wall_chart_data import (
    WallChartData, _from_excel_paramdocs,
)

from ..tools import (
    InputMissingError, WallChartsPatternError,
    Chart,
    open_sheets_doc,
    datetime_now,
    format_docstring,
)
from ..tools.pygsheets_tools.custom_pygsheets import _open_spreadsheet_paramdocs
from . import wall_chart_defaults as defaults

DocInfo = collections.namedtuple('DocInfo', ('docname', 'url', 'sheets'))


class WallMetadata(WallChartData):
    '''stores DataFrame containing metadata.
    Use WallMetadata.from_client to load from google doc,
    or WallMetadata.from_excel to load from excel.

    Supports writing to excel,
    but does not support editing google doc.
    '''
    NCOLS_MAX = 15
    # # # INIT CHECKS & CLEANUP # # #
    def _init_from(self, obj):
        '''called during __array_finalize__.'''
        super()._init_from(obj)
        self._check_sheet_name_correct()

    @classmethod
    def _new_arr_adjust(cls, arr, *, meta=dict()):
        '''adjust arr when creating new instance of cls.
        Adjustments here: _cleanup_cols; _trim_blank_departments.
        '''
        obj = super()._new_arr_adjust(arr, meta=meta)
        obj = obj._cleanup_cols()
        obj = obj._remove_blank_departments()
        return obj

    def _check_sheet_name_correct(self):
        '''if self has column 'sheet name correct?', check that all values are 'yes' or ''.
        if has that column but doesn't satisfy that check, raise WallChartsPatternError.
        '''
        # [TODO] why does using this code instead, cause RecursionError?
        # (it is related to the slicing requiring to call _init_from again.):
        # if 'sheet name correct?' in self.header:
        #     col = self.column('sheet name correct?')
        # (note: even just doing self[0, :] here will cause a recursion error.)
        # For now, getting around that bug via arr = np.asarray(self), first.
        arr = np.asarray(self)
        if 'sheet name correct?' in arr[0, :]:
            col = arr[1:, np.where(arr[0, :] == 'sheet name correct?')[0][0]]
            if ((col != 'yes') & (col != '')).any():
                errmsg = ('WallMetadata indicates at least one sheet name is not correct!'
                    '\nNote: this is a problem with the data, not the python code.'
                    "\nTo debug, look for any non-'yes' values in the 'sheet name correct' column,"
                    "\nof the metadata doc (whichever google sheets doc or excel doc you were trying to load).")
                raise WallChartsPatternError(errmsg)

    def _cleanup_cols(self):
        '''returns result of cleaning up columns of self.
        "cleaning up columns" means:
            first, take only the first self.NCOLS_MAX cols. (Use None to take all.)
            then, remove any fully blank columns (== self.FILL). (default FILL == '')
        '''
        result = self[:, :self.NCOLS_MAX]
        any_nonempty_in_col = np.any(result != self.FILL, axis=0)
        result = result[:, any_nonempty_in_col]
        return result

    def _remove_blank_departments(self):
        '''if self has column 'Department', remove all rows in self where Department == ''.'''
        if 'Department' in self.header:
            nonblanks = (self.column('Department', keep_header=True) != '')
            if not nonblanks.all():   # there is at least one blank
                return self[nonblanks, :]
        return self

    def _on_data_update(self):
        '''call when altering the data in self.
        overwrites super()'s method. Here, we only alter self.meta['_source'], not also 't_read'.
        '''
        self.meta['_source'] = None

    # # # ALTERNATE CONSTRUCTORS # # #
    @classmethod
    @format_docstring(paramdocs=_open_spreadsheet_paramdocs)
    def from_client(cls, client, *, sheet_name=None, **open_kwargs):
        '''return new WallMetadata object by reading from google docs.
        sheet_name: None or str
            sheet to load the info from.
            if None, use wall_chart_defaults.DEFAULT_META_NAME

        open_kwargs go to open_sheets_doc. They may be:
        {paramdocs}
        '''
        metaspread = open_sheets_doc(client, **open_kwargs)
        if sheet_name is None: sheet_name = defaults.DEFAULT_META_NAME
        worksheet = metaspread[sheet_name]
        chart_full = worksheet.get_chart(keep_formulas=False)
        t_read = datetime_now()
        return cls(chart_full, meta=dict(sheet_name=sheet_name, t_read=t_read))

    # # # EXCEL READ / WRITE # # #
    def _get_default_dst_excel(self):
        '''returns default (filename, sheet name) for saving self.'''
        name = str(self.sheet_name) if self.sheet_name is not None else defaults.DEFAULT_META_NAME
        t_read = self._get_t_read_string()
        return (f'{name}{t_read}.xlsx', name)

    def to_excel(self, dst=None, sheet_name=None, dir=None, **kw__write):
        '''saves meta to local machine at dst.
        if dst not provided, use self._get_default_dst_excel().
        if dir provided, save to that directory, making it if necessary.
        if dir not provided, try DEFAULT_SAVEDIR too.

        returns (abspath to saved file, sheet name)
        '''
        if 'Doc name' not in self.header:
            warnmsg = "'Doc name' column not provided. May want to set that, then save again."
            warnmsg += " E.g. via self.df['Doc name'] = self.load_doc_names()['Doc name']."
            warnings.warn(warnmsg)
        return super().to_excel(dst=dst, sheet_name=sheet_name, dir=dir, **kw__write)

    @classmethod
    @format_docstring(paramdocs=_from_excel_paramdocs)
    def from_excel(cls, filename=None, sheet_name=None, dir=None, choice_method='user', **kw__read):
        '''read sheet_name in excel file at filename, returning a WallMetadata object.
        
        if filename / sheet_name not input, guess wall_chart_defaults.DEFAULT_META_NAME
        '''
        if filename is None: filename = defaults.DEFAULT_META_NAME
        if sheet_name is None: sheet_name = defaults.DEFAULT_META_NAME
        kw_ = dict(filename=filename, sheet_name=sheet_name, dir=dir, choice_method=choice_method, **kw__read)
        return super().from_excel(**kw_)

    # # # DOC NAMES # # #
    def calculate_doc_names(self, client):
        '''use 'URL' column of self to learn the names where each wall chart is located.
        result is a Chart containing the columns 'URL' and 'Doc name'.
        If 'Department' is a column in self, result will also contain the column 'Department'.
        '''
        url_col = self.column('URL')
        url_to_name = {}  # cache results in case of repeated urls
        docnames = []
        L = len(url_col)
        for i, url in enumerate(url_col):
            print(f'url {i+1:2d} of {L}', end='\r')
            if url not in url_to_name.keys():
                doc = open_sheets_doc(client, url=url)
                url_to_name[url] = doc.title
            docnames.append(url_to_name[url])
        # organize those results into a Chart.
        r = dict()  # << for the result
        if 'Department' in self.header:
            r['Department'] = self.column('Department')
        r['URL'] = url_col
        r['Doc name'] = docnames
        return Chart.from_dict(r)

    def load_doc_names(self):
        '''returns DataFrame with columns ['Department', 'URL', 'Doc name'] if possible.
        If 'Department' not in columns of self, just give ['URL', 'Doc name'].
        If 'Doc name' not in columns of self, raise WallChartsPatternError.
        '''
        if 'URL' not in self.header:
            raise KeyError('URL')
        if 'Doc name' not in self.header:
            errmsg = ("'Doc name' column not found! Use self.calculate_doc_names() to calculate them."
                      " Recommended that you then update the sheet to put a 'Doc name' column."
                      "\nE.g. start using result=self.append_col_dict({'Doc name': "
                      "self.calculate_doc_names().column('Doc name')}) instead,"
                      "\nor update the sheet online, directly.")
            raise WallChartsPatternError(errmsg)
        colnames = ['URL', 'Doc name']
        if 'Department' in self.header: colnames = ['Department'] + colnames
        dict_ = {key: self.column(key) for key in colnames}
        return Chart.from_dict(dict_)

    def doc_to_url_dict(self):
        '''returns dict of {docname: url}.'''
        table = self.load_doc_names().to_df(row_0_as_header=True)
        table_unique = table.drop_duplicates(subset='Doc name')
        doc_and_url = table_unique[['Doc name', 'URL']]
        result = dict()
        for i, row in doc_and_url.iterrows():
            doc, url = row.values
            result[doc] = url
        return result

    def unique_doc_names(self, client=None):
        '''retuns list of doc names.'''
        table = self.load_doc_names().to_df(row_0_as_header=True)
        table_unique = table.drop_duplicates(subset='Doc name')
        return table_unique['Doc name'].values

    def list_docs(self):
        '''gets list of DocInfo (docname, url, sheets) for docname in self.'''
        table = self.to_df(row_0_as_header=True)
        table = table[['Doc name', 'URL', 'Sheet name']]
        docdict = collections.OrderedDict()
        for i, row in table.iterrows():
            doc, url, sheet = row
            key = (doc, url)
            try:
                docdict[key].append(sheet)
            except KeyError:
                docdict[key] = [sheet]
        result = [DocInfo(doc, url, tuple(sheets)) for ((doc, url), sheets) in docdict.items()]
        return result

    def iter_docs(self):
        '''iterates through self, yielding (docname, url, wallchart sheets in doc).'''
        # we need to go through all lines to make the list because there is one line per sheet,
        # so there may be sheets corresponding to earlier docs later in the list.
        # that's why the main code is in list_docs, and this is an alias to iter(self.list_docs(...)).
        return iter(self.list_docs())

    def matches_docs(self, other_meta):
        '''returns whether set(self.list_docs) == set(other_meta.list_docs).
        i.e. whether self and other meta list the same sheets on the same docs at the same urls.
        '''
        return set(self.list_docs()) == set(other_meta.list_docs())


    # # # INSPECT # # #
    def get_url(self, docname, *, _caching=True):
        '''returns url of this doc.
        _caching: bool
            whether to do caching.
            If True, save self.doc_to_url_dict() into self._cached_get_url__doc_to_url_dict,
            and pull urls from _cached_get_url__doc_to_url_dict.
            Never checks for updates to self; if self is changed later, results may be incorrect.
        '''
        if _caching:
            try:
                doc_to_url = self._cached_get_url__doc_to_url_dict
            except AttributeError:
                doc_to_url = self.doc_to_url_dict()
                self._cached_get_url__doc_to_url_dict = doc_to_url
            return doc_to_url[docname]
        else:
            chart = self.load_doc_names()
            wheredoc = np.where(chart.column('Doc name')==docname)[0]
            if len(wheredoc) == 0:
                raise KeyError(f'docname not found!: {repr(docname)}. See meta.unique_doc_names() for options.')
            return chart.column('URL')[wheredoc[0]]

    # # # LOOKUP DEPARTMENT NAMES # # #
    def dept2caddress(self):
        '''returns dict of {department name: (docname, sheetname)}'''
        return {dept: (doc, sheet) for (dept, doc, sheet) in self.columns('Department', 'Doc name', 'Sheet name')}

    def caddress2dept(self):
        '''returns dict of {(docname, sheetname): department name}'''
        return {(doc, sheet): dept for (dept, doc, sheet) in self.columns('Department', 'Doc name', 'Sheet name')}
